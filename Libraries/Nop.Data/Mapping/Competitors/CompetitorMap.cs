﻿using Nop.Core.Domain.Competitors;

namespace Nop.Data.Mapping.Competitors
{
    public partial class CompetitorMap : NopEntityTypeConfiguration<Competitor>
    {
        public CompetitorMap()
        {
            this.ToTable("Competitor");
            this.HasKey(v => v.Id);

            this.Property(v => v.Name).IsRequired().HasMaxLength(400);
            this.Property(v => v.Email).HasMaxLength(400);
            this.Property(v => v.MetaKeywords).HasMaxLength(400);
            this.Property(v => v.MetaTitle).HasMaxLength(400);
            this.Property(v => v.PageSizeOptions).HasMaxLength(200);
        }
    }
}

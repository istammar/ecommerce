﻿using Nop.Core.Domain.Competitors;

namespace Nop.Data.Mapping.Competitors
{
    public partial class CompetitorProductMap : NopEntityTypeConfiguration<CompetitorProduct>
    {
        public CompetitorProductMap()
        {
            this.ToTable("CompetitorProduct");
            this.HasKey(v => v.Id);
            this.Property(vn => vn.ProductId).IsRequired();
            this.Property(vn => vn.CompetitorId).IsRequired();
            this.Property(vn => vn.Price).HasPrecision(18, 4).IsRequired();
            this.Property(v => v.PageSizeOptions).HasMaxLength(200);

            this.HasRequired(vn => vn.Competitor)
                .WithMany(v => v.CompetitorProducts)
                .HasForeignKey(vn => vn.CompetitorId);

            this.HasRequired(vn => vn.Product)
                .WithMany(v => v.CompetitorProducts)
                .HasForeignKey(vn => vn.ProductId);
        }
    }
}

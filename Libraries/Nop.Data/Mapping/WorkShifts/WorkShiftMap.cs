﻿using Nop.Core.Domain.WorkShifts;

namespace Nop.Data.Mapping.WorkShifts
{
    public class WorkShiftMap : NopEntityTypeConfiguration<WorkShift>
    {
        public WorkShiftMap()
        {
            this.ToTable("WorkShift");
            this.HasKey(v => v.Id);

            this.Property(v => v.Name).IsRequired().HasMaxLength(50);
            this.Property(v => v.StartTime).IsRequired();
            this.Property(v => v.EndTime).IsRequired();
            this.Property(v => v.StoreId).IsRequired();
            this.Property(v => v.CreatedOnUtc).IsRequired();
            this.Property(v => v.UpdatedOnUtc).IsRequired();

            this.HasRequired(vn => vn.Store)
                .WithMany(v => v.WorkShifts)
                .HasForeignKey(vn => vn.StoreId);
        }
    }
}

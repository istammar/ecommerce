﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial  class ShoppingListItemMap : NopEntityTypeConfiguration<ShoppingListItem>
    {
        public ShoppingListItemMap()
        {
            this.ToTable("ShoppingListItem");
            this.HasKey(vn => vn.Id);

            this.Property(v => v.ProductId).IsRequired();
            this.Property(v => v.Quantity).IsRequired();

            this.HasRequired(vn => vn.ShoppingList)
                .WithMany(v => v.ShoppingListItems)
                .HasForeignKey(vn => vn.ShoppingListId);
        }
    }
}

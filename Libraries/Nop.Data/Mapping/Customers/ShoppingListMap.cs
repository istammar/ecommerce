﻿using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class ShoppingListMap : NopEntityTypeConfiguration<ShoppingList>
    {
        public ShoppingListMap()
        {
            this.ToTable("ShoppingList");
            this.HasKey(v => v.Id);

            this.Property(v => v.Name).IsRequired().HasMaxLength(400);
            this.Property(v => v.CustomerId).IsRequired();
            this.Property(v => v.CreatedOnUtc).IsRequired();
            this.Property(v => v.UpdatedOnUtc).IsRequired();
        }
    }
}

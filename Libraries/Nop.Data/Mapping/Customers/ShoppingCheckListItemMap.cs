﻿using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class ShoppingCheckListItemMap : NopEntityTypeConfiguration<ShoppingCheckListItem>
    {
        public ShoppingCheckListItemMap()
        {
            this.ToTable("ShoppingCheckListItem");
            this.HasKey(vn => vn.Id);

            this.Property(v => v.ProductType).IsRequired();
            this.Property(v => v.Quantity).IsRequired();

            this.HasRequired(vn => vn.ShoppingCheckList)
                .WithMany(v => v.ShoppingCheckListItems)
                .HasForeignKey(vn => vn.ShoppingCheckListId);
        }
    }
}

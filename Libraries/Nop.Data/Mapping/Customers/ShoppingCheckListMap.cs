﻿using Nop.Core.Domain.Customers;

namespace Nop.Data.Mapping.Customers
{
    public partial class ShoppingCheckListMap : NopEntityTypeConfiguration<ShoppingCheckList>
    {
        public ShoppingCheckListMap()
        {
            this.ToTable("ShoppingCheckList");
            this.HasKey(v => v.Id);

            this.Property(v => v.Name).IsRequired().HasMaxLength(400);
            this.Property(v => v.CustomerId).IsRequired();
            this.Property(v => v.Deleted).IsRequired();
            this.Property(v => v.CreatedOnUtc).IsRequired();
            this.Property(v => v.UpdatedOnUtc).IsRequired();
        }
    }
}

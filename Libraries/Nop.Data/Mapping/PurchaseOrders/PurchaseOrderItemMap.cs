﻿using Nop.Core.Domain.PurchaseOrders;

namespace Nop.Data.Mapping.PurchaseOrders
{
    public partial class PurchaseOrderItemMap : NopEntityTypeConfiguration<PurchaseOrderItem>
    {
        public PurchaseOrderItemMap()
        {
            this.ToTable("PurchaseOrderItem");
            this.HasKey(v => v.Id);
            this.Property(vn => vn.PurchaseOrderId).IsRequired();
            this.Property(vn => vn.ProductId).IsRequired();
            this.Property(p => p.ProductName).IsRequired().HasMaxLength(400);
            this.Property(p => p.ProductSku).HasMaxLength(400);
            this.Property(vn => vn.ProductQuantity).IsRequired();

            this.HasRequired(vn => vn.PurchaseOrder)
                .WithMany(v => v.PurchaseOrderItems)
                .HasForeignKey(vn => vn.PurchaseOrderId);

            this.HasRequired(vn => vn.Product)
                .WithMany(v => v.PurchaseOrderItems)
                .HasForeignKey(vn => vn.ProductId);
        }
    }
}

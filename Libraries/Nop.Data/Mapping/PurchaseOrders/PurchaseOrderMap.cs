﻿using Nop.Core.Domain.PurchaseOrders;

namespace Nop.Data.Mapping.PurchaseOrders
{
    public partial class PurchaseOrderMap : NopEntityTypeConfiguration<PurchaseOrder>
    {
        public PurchaseOrderMap()
        {
            this.ToTable("PurchaseOrder");
            this.HasKey(v => v.Id);

            this.Property(v => v.TotalQuantity).IsRequired();
        }
    }
}

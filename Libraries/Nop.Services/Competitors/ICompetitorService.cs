﻿using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Competitors;

namespace Nop.Services.Competitors
{
    public partial interface ICompetitorService
    {
        /// <summary>
        /// Gets a competitor by competitor identifier
        /// </summary>
        /// <param name="competitorId">Competitor identifier</param>
        /// <returns>Competitor</returns>
        Competitor GetCompetitorById(int competitorId);
        
        /// <summary>
        /// Gets a competitor by competitor identifier
        /// </summary>
        /// <param name="competitorName">Competitor identifier</param>
        /// <returns>Competitor</returns>
        Competitor GetCompetitorByName(string competitorName);

        /// <summary>
        /// Delete a competitor
        /// </summary>
        /// <param name="competitor">Competitor</param>
        void DeleteCompetitor(Competitor competitor);

        /// <summary>
        /// Inserts a competitor
        /// </summary>
        /// <param name="competitor">Competitor</param>
        void InsertCompetitor(Competitor competitor);

        /// <summary>
        /// Updates the competitor
        /// </summary>
        /// <param name="competitor">Competitor</param>
        void UpdateCompetitor(Competitor competitor);

        /// <summary>
        /// Gets all competitors
        /// </summary>
        /// <param name="name">Competitor name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Competitor</returns>
        IPagedList<Competitor> GetAllCompetitors(string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        IQueryable<Competitor> GetAllCompetitorList();
    }
}

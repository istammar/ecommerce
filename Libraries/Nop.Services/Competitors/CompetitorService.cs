﻿using System;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Competitors;
using Nop.Services.Events;
using System.Linq;

namespace Nop.Services.Competitors
{
    /// <summary>
    /// Competitor service
    /// </summary>
    public partial class CompetitorService : ICompetitorService
    {
        #region Fields

        private readonly IRepository<Competitor> _competitorRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="competitorRepository">Competitor repository</param>
        /// <param name="competitorProductRepository">Competitor product repository</param>
        /// <param name="eventPublisher">Event published</param>
        public CompetitorService(IRepository<Competitor> competitorRepository,
            IRepository<CompetitorProduct> competitorProductRepository,
            IEventPublisher eventPublisher)
        {
            this._competitorRepository = competitorRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        public Competitor GetCompetitorById(int competitorId)
        {
            if (competitorId == 0)
                return null;

            return _competitorRepository.GetById(competitorId);
        }

        public Competitor GetCompetitorByName(string competitorName)
        {
            if (string.IsNullOrWhiteSpace(competitorName))
                return null;

            competitorName = competitorName.Trim();

            return _competitorRepository.Table.FirstOrDefault(x => x.Name.ToLower() == competitorName.ToLower());
        }

        public void DeleteCompetitor(Competitor competitor)
        {
            if (competitor == null)
                throw new ArgumentNullException("competitor");

            competitor.Deleted = true;
            UpdateCompetitor(competitor);

            //event notification
            _eventPublisher.EntityDeleted(competitor);
        }

        public virtual void InsertCompetitor(Competitor competitor)
        {
            if (competitor == null)
                throw new ArgumentNullException("competitor");

            _competitorRepository.Insert(competitor);

            //event notification
            _eventPublisher.EntityInserted(competitor);
        }

        public virtual void UpdateCompetitor(Competitor competitor)
        {
            if (competitor == null)
                throw new ArgumentNullException("competitor");

            _competitorRepository.Update(competitor);

            //event notification
            _eventPublisher.EntityUpdated(competitor);
        }

        public virtual IPagedList<Competitor> GetAllCompetitors(string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = _competitorRepository.Table;
            if (!String.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));
            if (!showHidden)
                query = query.Where(v => v.Active);
            query = query.Where(v => !v.Deleted);
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name);

            var competitors = new PagedList<Competitor>(query, pageIndex, pageSize);
            return competitors;
        }

        public virtual IQueryable<Competitor> GetAllCompetitorList()
        {
            var query = _competitorRepository.Table;

            query = query.Where(v => v.Active);
            query = query.Where(v => !v.Deleted);
            query = query.OrderBy(v => v.DisplayOrder).ThenBy(v => v.Name);

            return query;
        }

        #endregion
    }
}

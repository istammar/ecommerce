﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.WorkShifts;

namespace Nop.Services.WorkShifts
{
    public partial interface IWorkShiftService
    {
        /// <summary>
        /// Gets a work shift by work shift identifier
        /// </summary>
        /// <param name="workShiftId">WorkShift identifier</param>
        /// <returns>Competitor</returns>
        WorkShift GetWorkShiftById(int workShiftId);

        /// <summary>
        /// Delete a work shift
        /// </summary>
        /// <param name="workShift">WorkShift</param>
        void DeleteWorkShift(WorkShift workShift);

        /// <summary>
        /// Inserts a work shift
        /// </summary>
        /// <param name="workShift">WorkShift</param>
        void InsertWorkShift(WorkShift workShift);

        /// <summary>
        /// Updates the work shift
        /// </summary>
        /// <param name="workShift">WorkShift</param>
        void UpdateWorkShift(WorkShift workShift);

        /// <summary>
        /// Gets all WorkShift
        /// </summary>
        /// <param name="name">Work Shift name</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>WorkShift</returns>
        IPagedList<WorkShift> GetAllWorkShifts(string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue);

        /// <summary>
        /// Gets all WorkShifts
        /// </summary>
        /// <returns>WorkShifts</returns>
        IList<WorkShift> GetAllWorkShifts();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.WorkShifts;
using Nop.Services.Events;

namespace Nop.Services.WorkShifts
{
    public partial class WorkShiftService : IWorkShiftService
    {
        #region Fields

        private readonly IRepository<WorkShift> _workShiftRepository;
        private readonly IEventPublisher _eventPublisher;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="workShiftRepository">Work Shift repository</param>
        /// <param name="eventPublisher">Event published</param>
        public WorkShiftService(IEventPublisher eventPublisher, IRepository<WorkShift> workShiftRepository)
        {
            this._workShiftRepository = workShiftRepository;
            this._eventPublisher = eventPublisher;

        }

        #endregion

        #region Methods

        public WorkShift GetWorkShiftById(int workShiftId)
        {
            if (workShiftId == 0)
                return null;

            return _workShiftRepository.GetById(workShiftId);
        }

        public void DeleteWorkShift(WorkShift workShift)
        {
            if (workShift == null)
                throw new ArgumentNullException("workShift");

            workShift.Published = true;
            UpdateWorkShift(workShift);

            //event notification
            _eventPublisher.EntityDeleted(workShift);
        }

        public void InsertWorkShift(WorkShift workShift)
        {
            if (workShift == null)
                throw new ArgumentNullException("workShift");

            _workShiftRepository.Insert(workShift);

            //event notification
            _eventPublisher.EntityInserted(workShift);
        }

        public void UpdateWorkShift(WorkShift workShift)
        {
            if (workShift == null)
                throw new ArgumentNullException("workShift");

            _workShiftRepository.Update(workShift);

            //event notification
            _eventPublisher.EntityUpdated(workShift);
        }

        public virtual IPagedList<WorkShift> GetAllWorkShifts(string name = "",
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _workShiftRepository.Table;
            if (!String.IsNullOrWhiteSpace(name))
                query = query.Where(v => v.Name.Contains(name));
            query = query.Where(v => v.Published);
            query = query.OrderBy(v => v.StartTime);

            var workShifts = new PagedList<WorkShift>(query, pageIndex, pageSize);
            return workShifts;
        }

        public virtual IList<WorkShift> GetAllWorkShifts()
        {
            var query = from s in _workShiftRepository.Table
                        where s.Published == true
                        orderby s.StartTime
                        select s;
            var workShifts = query.ToList();

            return workShifts;
        }

        #endregion
    }
}
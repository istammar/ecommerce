﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.PurchaseOrders;
using Nop.Services.Dto;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Competitors;
using Nop.Services.Events;
using Nop.Services.Orders;

namespace Nop.Services.PurchaseOrders
{
    public partial class PurchaseOrderService : IPurchaseOrderService
    {
        #region Fields

        private readonly IRepository<PurchaseOrder> _purchaseOrderRepository;
        private readonly IRepository<PurchaseOrderItem> _purchaseOrderItemRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IOrderProcessingService _orderProcessingService;

        #endregion

        #region Constructors

        public PurchaseOrderService(IRepository<PurchaseOrder> purchaseOrderRepository,
            IRepository<PurchaseOrderItem> purchaseOrderItemRepository,
            IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<Product> productRepository,
            IEventPublisher eventPublisher, IOrderProcessingService orderProcessingService)
        {
            this._purchaseOrderRepository = purchaseOrderRepository;
            this._purchaseOrderItemRepository = purchaseOrderItemRepository;
            this._orderRepository = orderRepository;
            this._orderItemRepository = orderItemRepository;
            this._productRepository = productRepository;
            this._eventPublisher = eventPublisher;
            _orderProcessingService = orderProcessingService;
        }

        #endregion

        #region Methods

        #region Purchase Order

        public PurchaseOrder GetPurchaseOrderById(int purchaseOrderId)
        {
            if (purchaseOrderId == 0)
                return null;

            return _purchaseOrderRepository.GetById(purchaseOrderId);
        }

        public void DeletePurchaseOrder(PurchaseOrder purchaseOrder)
        {
            if (purchaseOrder == null)
                throw new ArgumentNullException("purchaseOrder");

            purchaseOrder.Deleted = true;
            UpdatePurchaseOrder(purchaseOrder);

            //event notification
            _eventPublisher.EntityDeleted(purchaseOrder);
        }

        public virtual void InsertPurchaseOrder(PurchaseOrder purchaseOrder)
        {
            if (purchaseOrder == null)
                throw new ArgumentNullException("purchaseOrder");

            _purchaseOrderRepository.Insert(purchaseOrder);

            //event notification
            _eventPublisher.EntityInserted(purchaseOrder);
        }

        public virtual void UpdatePurchaseOrder(PurchaseOrder purchaseOrder)
        {
            if (purchaseOrder == null)
                throw new ArgumentNullException("purchaseOrder");

            _purchaseOrderRepository.Update(purchaseOrder);

            //event notification
            _eventPublisher.EntityUpdated(purchaseOrder);
        }

        #endregion

        #region Purchase Order Item

        public virtual void InsertPurchaseOrderItem(PurchaseOrderItem purchaseOrderItem)
        {
            if (purchaseOrderItem == null)
                throw new ArgumentNullException("purchaseOrderItem");

            _purchaseOrderItemRepository.Insert(purchaseOrderItem);

            //event notification
            _eventPublisher.EntityInserted(purchaseOrderItem);
        }

        #endregion


        public void GeneratePurchaseOrder(List<int> orderIds)
        {
            var orderItems = (from o in _orderRepository.Table
                join oi in _orderItemRepository.Table on o.Id equals oi.OrderId
                join product in _productRepository.Table on oi.ProductId equals product.Id
                where (orderIds.Count == 0 || orderIds.Contains(o.Id))
                select new
                {
                    Product = product,
                    Quantity = oi.Quantity
                }).AsEnumerable()
                .GroupBy(x => x.Product)
                .Select(x => new MergedOrderItemDto
                {
                    Product = x.Key,
                    Quantity = x.Sum(y => y.Quantity)
                }).ToList();

            // Create Purchase Order
            PurchaseOrder po = new PurchaseOrder
            {
                Description = "Auto Generated Description",
                TotalQuantity = orderItems.Sum(x => x.Quantity),
                Deleted = false,
                CreatedOnUtc = DateTime.UtcNow,
                UpdatedOnUtc = DateTime.UtcNow
            };

            InsertPurchaseOrder(po);

            // Create Purchase Order Item(s)
            foreach (var orderItemDto in orderItems)
            {
                PurchaseOrderItem poItem = new PurchaseOrderItem
                {
                    PurchaseOrderId = po.Id,
                    ProductId = orderItemDto.Product.Id,
                    ProductName = orderItemDto.Product.Name,
                    ProductSku = orderItemDto.Product.Sku,
                    ProductQuantity = orderItemDto.Quantity,
                    VendorId = orderItemDto.Product.VendorId,
                };

                InsertPurchaseOrderItem(poItem);
            }

            // Update Order(s) status to "Processing"
            foreach (var orderId in orderIds)
            {
                Order order = _orderRepository.GetById(orderId);
                order.PurchaseOrderId = po.Id;
                _orderProcessingService.ProcessOrder(order);
            }
        }

        public IPagedList<PurchaseOrder> GetPurchaseOrderList(int pageIndex = 0, int pageSize = int.MaxValue,
            bool showHidden = false)
        {
            var query = _purchaseOrderRepository.Table;
            query = query.Where(v => !v.Deleted);
            query = query.OrderByDescending(v => v.CreatedOnUtc);

            var purchaseOrders = new PagedList<PurchaseOrder>(query, pageIndex, pageSize);
            return purchaseOrders;
        }

        #endregion
    }
}

﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.PurchaseOrders;

namespace Nop.Services.PurchaseOrders
{
    public partial interface IPurchaseOrderService
    {
        /// <summary>
        /// Gets a Purchase Order by Purchase Order identifier
        /// </summary>
        /// <param name="purchaseOrderId">Purchase Order identifier</param>
        /// <returns>PurchaseOrder</returns>
        PurchaseOrder GetPurchaseOrderById(int purchaseOrderId);

        /// <summary>
        /// Delete a Purchase Order
        /// </summary>
        /// <param name="purchaseOrder">PurchaseOrder</param>
        void DeletePurchaseOrder(PurchaseOrder purchaseOrder);

        /// <summary>
        /// Inserts a Purchase Order
        /// </summary>
        /// <param name="purchaseOrder">PurchaseOrder</param>
        void InsertPurchaseOrder(PurchaseOrder purchaseOrder);

        /// <summary>
        /// Updates the Purchase Order
        /// </summary>
        /// <param name="purchaseOrder">PurchaseOrder</param>
        void UpdatePurchaseOrder(PurchaseOrder purchaseOrder);

        void GeneratePurchaseOrder(List<int> orderIds);

        IPagedList<PurchaseOrder> GetPurchaseOrderList(int pageIndex = 0, int pageSize = int.MaxValue,
            bool showHidden = false);
    }
}

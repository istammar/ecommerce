﻿using System;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Services.Events;

namespace Nop.Services.Customers
{
    public partial class ShoppingCheckListItemService : IShoppingCheckListItemService
    {

        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<ShoppingCheckListItem> _shoppingCheckListItemRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="shoppingCheckListItemRepository">Shopping Check List Item repository</param>
        public ShoppingCheckListItemService(IEventPublisher eventPublisher, IRepository<ShoppingCheckListItem> shoppingCheckListItemRepository)
        {
            this._eventPublisher = eventPublisher;
            this._shoppingCheckListItemRepository = shoppingCheckListItemRepository;
        }

        #endregion

        #region Methods

        public void InsertShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem)
        {
            if (shoppingCheckListItem == null)
                throw new ArgumentNullException("shoppingCheckListItem");

            _shoppingCheckListItemRepository.Insert(shoppingCheckListItem);

            //event notification
            _eventPublisher.EntityInserted(shoppingCheckListItem);
        }

        public void UpdateShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem)
        {
            if (shoppingCheckListItem == null)
                throw new ArgumentNullException("shoppingCheckListItem");

            _shoppingCheckListItemRepository.Update(shoppingCheckListItem);

            //event notification
            _eventPublisher.EntityUpdated(shoppingCheckListItem);
        }

        public void DeleteShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem)
        {
            if (shoppingCheckListItem == null)
                throw new ArgumentNullException("shoppingCheckListItem");

            shoppingCheckListItem.Deleted = true;
            UpdateShoppingCheckListItem(shoppingCheckListItem);

            //event notification
            _eventPublisher.EntityDeleted(shoppingCheckListItem);
        }

        public ShoppingCheckListItem GetShoppingCheckListItemById(int id)
        {
            return _shoppingCheckListItemRepository.GetById(id);
        }

        public void UpdateShoppingCheckListItemQuantity(int id, int updatedQuantity)
        {
            ShoppingCheckListItem checkListItem = _shoppingCheckListItemRepository.GetById(id);

            if (checkListItem != null)
            {
                checkListItem.Quantity = updatedQuantity;
                _shoppingCheckListItemRepository.Update(checkListItem);

                //event notification
                _eventPublisher.EntityUpdated(checkListItem);
            }
        }

        #endregion
    }
}

﻿using Nop.Core.Domain.Customers;

namespace Nop.Services.Customers
{
    public partial interface IShoppingCheckListItemService
    {
        void InsertShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem);

        void UpdateShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem);

        void DeleteShoppingCheckListItem(ShoppingCheckListItem shoppingCheckListItem);

        ShoppingCheckListItem GetShoppingCheckListItemById(int id);

        void UpdateShoppingCheckListItemQuantity(int id, int updatedQuantity);
    }
}

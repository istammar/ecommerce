﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core.Data;
using Nop.Core.Domain.Customers;
using Nop.Services.Events;

namespace Nop.Services.Customers
{
    public partial class ShoppingCheckListService : IShoppingCheckListService
    {
        #region Fields

        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<ShoppingCheckList> _shoppingCheckListRepository;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="shoppingCheckListRepository">Shopping Check List repository</param>
        public ShoppingCheckListService(IEventPublisher eventPublisher, IRepository<ShoppingCheckList> shoppingCheckListRepository)
        {
            this._eventPublisher = eventPublisher;
            this._shoppingCheckListRepository = shoppingCheckListRepository;
        }

        #endregion

        #region Methods

        public void InsertShoppingCheckList(ShoppingCheckList shoppingCheckList)
        {
            if (shoppingCheckList == null)
                throw new ArgumentNullException("shoppingCheckList");

            _shoppingCheckListRepository.Insert(shoppingCheckList);

            //event notification
            _eventPublisher.EntityInserted(shoppingCheckList);
        }

        public void UpdateShoppingCheckList(ShoppingCheckList shoppingCheckList)
        {
            if (shoppingCheckList == null)
                throw new ArgumentNullException("shoppingCheckList");

            _shoppingCheckListRepository.Update(shoppingCheckList);

            //event notification
            _eventPublisher.EntityUpdated(shoppingCheckList);
        }

        public void DeleteShoppingCheckList(ShoppingCheckList shoppingCheckList)
        {
            if (shoppingCheckList == null)
                throw new ArgumentNullException("shoppingCheckList");

            shoppingCheckList.Deleted = true;
            UpdateShoppingCheckList(shoppingCheckList);

            //event notification
            _eventPublisher.EntityDeleted(shoppingCheckList);
        }

        public ShoppingCheckList GetShoppingCheckListById(int id)
        {
            return _shoppingCheckListRepository.GetById(id);
        }

        public List<ShoppingCheckList> GetShoppingCheckListsByCustomerId(int customerId)
        {
            if (customerId == 0)
                return null;

            return _shoppingCheckListRepository.Table.Where(x => x.CustomerId == customerId && x.Deleted == false).ToList();
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Services.Catalog;
using Nop.Services.Dto;
using Nop.Services.Events;

namespace Nop.Services.Customers
{
    public partial class ShoppingListService : IShoppingListService
    {
        private readonly IRepository<ShoppingList> _shoppingListRepository;
        private readonly IRepository<ShoppingListItem> _shoppingListItemRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Product> _productRepository;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IWorkContext _workContext;

        public ShoppingListService(IRepository<ShoppingList> shoppingListRepository,
            IRepository<ShoppingListItem> shoppingListItemRepository,
            IEventPublisher eventPublisher, IRepository<Product> productRepository, IPriceFormatter priceFormatter, IWorkContext workContext)
        {
            this._shoppingListRepository = shoppingListRepository;
            this._eventPublisher = eventPublisher;
            this._shoppingListItemRepository = shoppingListItemRepository;
            this._productRepository = productRepository;
            _priceFormatter = priceFormatter;
            _workContext = workContext;
        }

        public List<ShoppingListDto> GetShoppingList(int customerId)
        {
            /*List<ShoppingListDto> lstShoppingLists = (from shoppingList in _shoppingListRepository.Table
                                                      join listItem in _shoppingListItemRepository.Table
                                                          on shoppingList.Id equals listItem.ShoppingListId into slij
                                                      from shoppingListItem in slij.DefaultIfEmpty()
                                                      where shoppingList.CustomerId == customerId
                                                      group shoppingListItem by shoppingList into odg
                                                      select new ShoppingListDto()
                                                      {
                                                          Id = odg.Key.Id,
                                                          Name = odg.Key.Name,
                                                          ProductQuantity = odg.Sum(x => x.Quantity)
                                                      }).ToList();*/
            List<ShoppingListDto> lstShoppingLists =
                _shoppingListRepository.Table.Where(x => x.CustomerId == customerId).Select(x => new ShoppingListDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    ProductQuantity = x.ShoppingListItems.Count
                }).ToList();
            return lstShoppingLists;
        }

        public List<ShoppingEditListDto> GetShoppingListItemsById(int shoppingListId)
        {
            if (shoppingListId == 0)
                return null;
            var tempShoppingList = (from shoppingList in _shoppingListRepository.Table
                                    join shoppingListItem in _shoppingListItemRepository.Table
                                        on shoppingList.Id equals shoppingListItem.ShoppingListId
                                    join product in _productRepository.Table
                                        on shoppingListItem.ProductId equals product.Id
                                    where shoppingList.Id == shoppingListId
                                    select new ShoppingEditListDto
                                    {
                                        Id = shoppingListItem.Id,
                                        ProductId = shoppingListItem.ProductId,
                                        Name = product.Name,
                                        Quantity = shoppingListItem.Quantity,
                                        Price = product.Price,
                                        Total = shoppingListItem.Quantity * product.Price,
                                    }).ToList();

            var shoppingListReturn = tempShoppingList.Select(x => new ShoppingEditListDto
            {
                Id = x.Id,
                ProductId = x.ProductId,
                Name = x.Name,
                Quantity = x.Quantity,
                Price = x.Price,
                PriceString = _priceFormatter.FormatPrice(x.Price, true, _workContext.WorkingCurrency),
                Total = x.Quantity * x.Price,
                TotalString =
                    _priceFormatter.FormatPrice((x.Quantity * x.Price), true,
                        _workContext.WorkingCurrency)
            }).ToList();

            return shoppingListReturn;
        }

        public void InsertShoppingList(ShoppingList shoppingList)
        {
            if (shoppingList == null)
                throw new ArgumentNullException("shoppingList");

            _shoppingListRepository.Insert(shoppingList);

            //event notification
            _eventPublisher.EntityInserted(shoppingList);
        }

        //public void UpdateShoppingList(ShoppingList shoppingList)
        //{
        //    if (shoppingList == null)
        //        throw new ArgumentNullException("shoppingList");

        //    this.DeleteShoppingList(shoppingList.Id);
        //    shoppingList.Id = 0;

        //    _shoppingListRepository.Insert(shoppingList);

        //    //event notification
        //    _eventPublisher.EntityUpdated(shoppingList);
        //}

        public void UpdateShoppingList(ShoppingList shoppingList)
        {
            if (shoppingList == null)
            {
                throw new ArgumentNullException("shoppingList");
            }

            ShoppingList shoppingListToUpdate = _shoppingListRepository.GetById(shoppingList.Id);

            shoppingListToUpdate.Name = shoppingList.Name;
            shoppingListToUpdate.UpdatedOnUtc = DateTime.UtcNow;

            _shoppingListRepository.Update(shoppingList);

            //event notification
            _eventPublisher.EntityUpdated(shoppingList);
        }

        public void DeleteShoppingList(int id)
        {
            var entity = _shoppingListRepository.GetById(id);

            var listItems = (from sl in _shoppingListRepository.Table
                             join sli in _shoppingListItemRepository.Table
                             on sl.Id equals sli.ShoppingListId
                             where sl.Id == id
                             select sli).ToList();

            foreach (var item in listItems)
            {
                _shoppingListItemRepository.Delete(item);
            }

            _shoppingListRepository.Delete(entity);

            //event notification
            _eventPublisher.EntityDeleted(entity);
        }

        public ShoppingList GetShoppingListById(int id)
        {
            return _shoppingListRepository.GetById(id);
        }

        public void UpdateShoppingListItems(int shoppingListId, List<ShoppingListItem> shoppingListItems)
        {
            ICollection<ShoppingListItem> dbShoppingListItems = _shoppingListRepository.GetById(shoppingListId).ShoppingListItems;
            List<int> dbItemIds = dbShoppingListItems.Select(x => x.Id).ToList();

            foreach (var item in shoppingListItems)
            {
                if (dbItemIds.Contains(item.Id))
                {
                    // update existing item
                    var itemToUpdate = dbShoppingListItems.First(x => x.Id == item.Id);
                    itemToUpdate.Quantity = item.Quantity;
                    _shoppingListItemRepository.Update(itemToUpdate);
                    dbItemIds.Remove(item.Id);
                }
                else
                {
                    // Add new item
                    _shoppingListItemRepository.Insert(item);
                }
            }

            if (dbItemIds.Any())
            {
                foreach (var itemId in dbItemIds)
                {
                    _shoppingListItemRepository.Delete(dbShoppingListItems.First(x => x.Id == itemId));
                }
            }
        }
    }
}

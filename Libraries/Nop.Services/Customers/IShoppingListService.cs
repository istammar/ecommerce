﻿using System.Collections.Generic;
using Nop.Core.Domain.Customers;
using Nop.Services.Dto;

namespace Nop.Services.Customers
{
    public partial interface IShoppingListService
    {
        ShoppingList GetShoppingListById(int id);

        List<ShoppingEditListDto> GetShoppingListItemsById(int id);

        List<ShoppingListDto> GetShoppingList(int customerId);

        void DeleteShoppingList(int id);

        void InsertShoppingList(ShoppingList shoppingList);

        void UpdateShoppingList(ShoppingList shoppingList);

        void UpdateShoppingListItems(int shoppingListId, List<ShoppingListItem> shoppingListItems);
    }
}

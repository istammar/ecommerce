﻿using System.Collections.Generic;
using Nop.Core.Domain.Customers;

namespace Nop.Services.Customers
{
    public partial interface IShoppingCheckListService
    {
        void InsertShoppingCheckList(ShoppingCheckList shoppingCheckList);

        void UpdateShoppingCheckList(ShoppingCheckList shoppingCheckList);

        void DeleteShoppingCheckList(ShoppingCheckList shoppingCheckList);

        ShoppingCheckList GetShoppingCheckListById(int id);

        List<ShoppingCheckList> GetShoppingCheckListsByCustomerId(int customerId);
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.WorkShifts;
using Nop.Core.Infrastructure;
using Nop.Services.Dto;
using Nop.Services.Events;
using Nop.Services.Helpers;

namespace Nop.Services.Orders
{
    /// <summary>
    /// Order service
    /// </summary>
    public partial class OrderService : IOrderService
    {
        #region Fields

        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<OrderItem> _orderItemRepository;
        private readonly IRepository<OrderNote> _orderNoteRepository;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<RecurringPayment> _recurringPaymentRepository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<WorkShift> _workShiftRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="orderRepository">Order repository</param>
        /// <param name="orderItemRepository">Order item repository</param>
        /// <param name="orderNoteRepository">Order note repository</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="recurringPaymentRepository">Recurring payment repository</param>
        /// <param name="customerRepository">Customer repository</param>
        /// <param name="eventPublisher">Event published</param>
        public OrderService(IRepository<Order> orderRepository,
            IRepository<OrderItem> orderItemRepository,
            IRepository<OrderNote> orderNoteRepository,
            IRepository<Product> productRepository,
            IRepository<RecurringPayment> recurringPaymentRepository,
            IRepository<Customer> customerRepository,
            IEventPublisher eventPublisher, IRepository<WorkShift> workShiftRepository, IDateTimeHelper dateTimeHelper)
        {
            this._orderRepository = orderRepository;
            this._orderItemRepository = orderItemRepository;
            this._orderNoteRepository = orderNoteRepository;
            this._productRepository = productRepository;
            this._recurringPaymentRepository = recurringPaymentRepository;
            this._customerRepository = customerRepository;
            this._eventPublisher = eventPublisher;
            _workShiftRepository = workShiftRepository;
            _dateTimeHelper = dateTimeHelper;
        }

        #endregion

        #region Methods

        #region Orders

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderId">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderById(int orderId)
        {
            if (orderId == 0)
                return null;

            return _orderRepository.GetById(orderId);
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="customOrderNumber">The custom order number</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByCustomOrderNumber(string customOrderNumber)
        {
            if (string.IsNullOrEmpty(customOrderNumber))
                return null;

            return _orderRepository.Table.FirstOrDefault(o => o.CustomOrderNumber == customOrderNumber);
        }

        /// <summary>
        /// Get orders by identifiers
        /// </summary>
        /// <param name="orderIds">Order identifiers</param>
        /// <returns>Order</returns>
        public virtual IList<Order> GetOrdersByIds(int[] orderIds)
        {
            if (orderIds == null || orderIds.Length == 0)
                return new List<Order>();

            var query = from o in _orderRepository.Table
                        where orderIds.Contains(o.Id) && !o.Deleted
                        select o;
            var orders = query.ToList();
            //sort by passed identifiers
            var sortedOrders = new List<Order>();
            foreach (int id in orderIds)
            {
                var order = orders.Find(x => x.Id == id);
                if (order != null)
                    sortedOrders.Add(order);
            }
            return sortedOrders;
        }

        /// <summary>
        /// Gets an order
        /// </summary>
        /// <param name="orderGuid">The order identifier</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByGuid(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
                return null;

            var query = from o in _orderRepository.Table
                        where o.OrderGuid == orderGuid
                        select o;
            var order = query.FirstOrDefault();
            return order;
        }

        /// <summary>
        /// Deletes an order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void DeleteOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            order.Deleted = true;
            UpdateOrder(order);

            //event notification
            _eventPublisher.EntityDeleted(order);
        }

        /// <summary>
        /// Search orders
        /// </summary>
        /// <param name="storeId">Store identifier; 0 to load all orders</param>
        /// <param name="vendorId">Vendor identifier; null to load all orders</param>
        /// <param name="customerId">Customer identifier; 0 to load all orders</param>
        /// <param name="productId">Product identifier which was purchased in an order; 0 to load all orders</param>
        /// <param name="affiliateId">Affiliate identifier; 0 to load all orders</param>
        /// <param name="billingCountryId">Billing country identifier; 0 to load all orders</param>
        /// <param name="warehouseId">Warehouse identifier, only orders with products from a specified warehouse will be loaded; 0 to load all orders</param>
        /// <param name="paymentMethodSystemName">Payment method system name; null to load all records</param>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="osIds">Order status identifiers; null to load all orders</param>
        /// <param name="psIds">Payment status identifiers; null to load all orders</param>
        /// <param name="ssIds">Shipping status identifiers; null to load all orders</param>
        /// <param name="billingEmail">Billing email. Leave empty to load all records.</param>
        /// <param name="billingLastName">Billing last name. Leave empty to load all records.</param>
        /// <param name="orderNotes">Search in order notes. Leave empty to load all records.</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// /// <param name="purchaseOrderId">Purchase Order Id</param>
        /// <returns>Orders</returns>
        public virtual IPagedList<Order> SearchOrders(int storeId = 0,
            int vendorId = 0, int customerId = 0,
            int productId = 0, int affiliateId = 0, int warehouseId = 0,
            int billingCountryId = 0, string paymentMethodSystemName = null,
            DateTime? createdFromUtc = null, DateTime? createdToUtc = null,
            List<int> osIds = null, List<int> psIds = null, List<int> ssIds = null,
            string billingEmail = null, string billingLastName = "",
            string orderNotes = null, int pageIndex = 0, int pageSize = int.MaxValue,
            int? purchaseOrderId = null)
        {
            var query = _orderRepository.Table;
            if (storeId > 0)
                query = query.Where(o => o.StoreId == storeId);
            if (vendorId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                        .Any(orderItem => orderItem.Product.VendorId == vendorId));
            }
            if (customerId > 0)
                query = query.Where(o => o.CustomerId == customerId);
            if (productId > 0)
            {
                query = query
                    .Where(o => o.OrderItems
                        .Any(orderItem => orderItem.Product.Id == productId));
            }
            if (warehouseId > 0)
            {
                var manageStockInventoryMethodId = (int)ManageInventoryMethod.ManageStock;
                query = query
                    .Where(o => o.OrderItems
                        .Any(orderItem =>
                            //"Use multiple warehouses" enabled
                            //we search in each warehouse
                            (orderItem.Product.ManageInventoryMethodId == manageStockInventoryMethodId &&
                             orderItem.Product.UseMultipleWarehouses &&
                             orderItem.Product.ProductWarehouseInventory.Any(pwi => pwi.WarehouseId == warehouseId))
                            ||
                            //"Use multiple warehouses" disabled
                            //we use standard "warehouse" property
                            ((orderItem.Product.ManageInventoryMethodId != manageStockInventoryMethodId ||
                              !orderItem.Product.UseMultipleWarehouses) &&
                             orderItem.Product.WarehouseId == warehouseId))
                    );
            }
            if (billingCountryId > 0)
                query = query.Where(o => o.BillingAddress != null && o.BillingAddress.CountryId == billingCountryId);
            if (!String.IsNullOrEmpty(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);
            if (affiliateId > 0)
                query = query.Where(o => o.AffiliateId == affiliateId);
            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);
            if (osIds != null && osIds.Any())
                query = query.Where(o => osIds.Contains(o.OrderStatusId));
            if (psIds != null && psIds.Any())
                query = query.Where(o => psIds.Contains(o.PaymentStatusId));
            if (ssIds != null && ssIds.Any())
                query = query.Where(o => ssIds.Contains(o.ShippingStatusId));
            if (!String.IsNullOrEmpty(billingEmail))
                query =
                    query.Where(
                        o =>
                            o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.Email) &&
                            o.BillingAddress.Email.Contains(billingEmail));
            if (!String.IsNullOrEmpty(billingLastName))
                query =
                    query.Where(
                        o =>
                            o.BillingAddress != null && !String.IsNullOrEmpty(o.BillingAddress.LastName) &&
                            o.BillingAddress.LastName.Contains(billingLastName));
            if (!String.IsNullOrEmpty(orderNotes))
                query = query.Where(o => o.OrderNotes.Any(on => on.Note.Contains(orderNotes)));
            query = query.Where(o => !o.Deleted);
            query = query.OrderByDescending(o => o.CreatedOnUtc);

            if (purchaseOrderId != null)
            {
                query = query
                    .Where(o => o.PurchaseOrderId == purchaseOrderId);
            }

            //database layer paging
            return new PagedList<Order>(query, pageIndex, pageSize);
        }

        /// <summary>
        /// Inserts an order
        /// </summary>
        /// <param name="order">Order</param>
        public virtual void InsertOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orderRepository.Insert(order);

            //event notification
            _eventPublisher.EntityInserted(order);
        }

        /// <summary>
        /// Updates the order
        /// </summary>
        /// <param name="order">The order</param>
        public virtual void UpdateOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            _orderRepository.Update(order);

            //event notification
            _eventPublisher.EntityUpdated(order);
        }

        /// <summary>
        /// Get an order by authorization transaction ID and payment method system name
        /// </summary>
        /// <param name="authorizationTransactionId">Authorization transaction ID</param>
        /// <param name="paymentMethodSystemName">Payment method system name</param>
        /// <returns>Order</returns>
        public virtual Order GetOrderByAuthorizationTransactionIdAndPaymentMethod(string authorizationTransactionId,
            string paymentMethodSystemName)
        {
            var query = _orderRepository.Table;
            if (!String.IsNullOrWhiteSpace(authorizationTransactionId))
                query = query.Where(o => o.AuthorizationTransactionId == authorizationTransactionId);

            if (!String.IsNullOrWhiteSpace(paymentMethodSystemName))
                query = query.Where(o => o.PaymentMethodSystemName == paymentMethodSystemName);

            query = query.OrderByDescending(o => o.CreatedOnUtc);
            var order = query.FirstOrDefault();
            return order;
        }

        #endregion

        #region Orders items

        /// <summary>
        /// Gets an order item
        /// </summary>
        /// <param name="orderItemId">Order item identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemById(int orderItemId)
        {
            if (orderItemId == 0)
                return null;

            return _orderItemRepository.GetById(orderItemId);
        }

        /// <summary>
        /// Gets an item
        /// </summary>
        /// <param name="orderItemGuid">Order identifier</param>
        /// <returns>Order item</returns>
        public virtual OrderItem GetOrderItemByGuid(Guid orderItemGuid)
        {
            if (orderItemGuid == Guid.Empty)
                return null;

            var query = from orderItem in _orderItemRepository.Table
                        where orderItem.OrderItemGuid == orderItemGuid
                        select orderItem;
            var item = query.FirstOrDefault();
            return item;
        }

        /// <summary>
        /// Gets all downloadable order items
        /// </summary>
        /// <param name="customerId">Customer identifier; null to load all records</param>
        /// <returns>Order items</returns>
        public virtual IList<OrderItem> GetDownloadableOrderItems(int customerId)
        {
            if (customerId == 0)
                throw new ArgumentOutOfRangeException("customerId");

            var query = from orderItem in _orderItemRepository.Table
                        join o in _orderRepository.Table on orderItem.OrderId equals o.Id
                        join p in _productRepository.Table on orderItem.ProductId equals p.Id
                        where customerId == o.CustomerId &&
                              p.IsDownload &&
                              !o.Deleted
                        orderby o.CreatedOnUtc descending, orderItem.Id
                        select orderItem;

            var orderItems = query.ToList();
            return orderItems;
        }

        /// <summary>
        /// Delete an order item
        /// </summary>
        /// <param name="orderItem">The order item</param>
        public virtual void DeleteOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException("orderItem");

            _orderItemRepository.Delete(orderItem);

            //event notification
            _eventPublisher.EntityDeleted(orderItem);
        }

        #endregion

        #region Orders notes

        /// <summary>
        /// Gets an order note
        /// </summary>
        /// <param name="orderNoteId">The order note identifier</param>
        /// <returns>Order note</returns>
        public virtual OrderNote GetOrderNoteById(int orderNoteId)
        {
            if (orderNoteId == 0)
                return null;

            return _orderNoteRepository.GetById(orderNoteId);
        }

        /// <summary>
        /// Deletes an order note
        /// </summary>
        /// <param name="orderNote">The order note</param>
        public virtual void DeleteOrderNote(OrderNote orderNote)
        {
            if (orderNote == null)
                throw new ArgumentNullException("orderNote");

            _orderNoteRepository.Delete(orderNote);

            //event notification
            _eventPublisher.EntityDeleted(orderNote);
        }

        #endregion

        #region Recurring payments

        /// <summary>
        /// Deletes a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void DeleteRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException("recurringPayment");

            recurringPayment.Deleted = true;
            UpdateRecurringPayment(recurringPayment);

            //event notification
            _eventPublisher.EntityDeleted(recurringPayment);
        }

        /// <summary>
        /// Gets a recurring payment
        /// </summary>
        /// <param name="recurringPaymentId">The recurring payment identifier</param>
        /// <returns>Recurring payment</returns>
        public virtual RecurringPayment GetRecurringPaymentById(int recurringPaymentId)
        {
            if (recurringPaymentId == 0)
                return null;

            return _recurringPaymentRepository.GetById(recurringPaymentId);
        }

        /// <summary>
        /// Inserts a recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void InsertRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException("recurringPayment");

            _recurringPaymentRepository.Insert(recurringPayment);

            //event notification
            _eventPublisher.EntityInserted(recurringPayment);
        }

        /// <summary>
        /// Updates the recurring payment
        /// </summary>
        /// <param name="recurringPayment">Recurring payment</param>
        public virtual void UpdateRecurringPayment(RecurringPayment recurringPayment)
        {
            if (recurringPayment == null)
                throw new ArgumentNullException("recurringPayment");

            _recurringPaymentRepository.Update(recurringPayment);

            //event notification
            _eventPublisher.EntityUpdated(recurringPayment);
        }

        /// <summary>
        /// Search recurring payments
        /// </summary>
        /// <param name="storeId">The store identifier; 0 to load all records</param>
        /// <param name="customerId">The customer identifier; 0 to load all records</param>
        /// <param name="initialOrderId">The initial order identifier; 0 to load all records</param>
        /// <param name="initialOrderStatus">Initial order status identifier; null to load all records</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Recurring payments</returns>
        public virtual IPagedList<RecurringPayment> SearchRecurringPayments(int storeId = 0,
            int customerId = 0, int initialOrderId = 0, OrderStatus? initialOrderStatus = null,
            int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            int? initialOrderStatusId = null;
            if (initialOrderStatus.HasValue)
                initialOrderStatusId = (int)initialOrderStatus.Value;

            var query1 = from rp in _recurringPaymentRepository.Table
                         join c in _customerRepository.Table on rp.InitialOrder.CustomerId equals c.Id
                         where
                             (!rp.Deleted) &&
                             (showHidden || !rp.InitialOrder.Deleted) &&
                             (showHidden || !c.Deleted) &&
                             (showHidden || rp.IsActive) &&
                             (customerId == 0 || rp.InitialOrder.CustomerId == customerId) &&
                             (storeId == 0 || rp.InitialOrder.StoreId == storeId) &&
                             (initialOrderId == 0 || rp.InitialOrder.Id == initialOrderId) &&
                             (!initialOrderStatusId.HasValue || initialOrderStatusId.Value == 0 ||
                              rp.InitialOrder.OrderStatusId == initialOrderStatusId.Value)
                         select rp.Id;

            var query2 = from rp in _recurringPaymentRepository.Table
                         where query1.Contains(rp.Id)
                         orderby rp.StartDateUtc, rp.Id
                         select rp;

            var recurringPayments = new PagedList<RecurringPayment>(query2, pageIndex, pageSize);
            return recurringPayments;
        }

        #endregion

        #region Purchase Order

        //public IPagedList<PurchaseOrderDto> GetPurchaseOrder(DateTime? dateTime, string productName = "",
        //    int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        //{
        //    var purchaseOrders = (from o in _orderRepository.Table
        //                          join oi in _orderItemRepository.Table on o.Id equals oi.OrderId
        //                          join product in _productRepository.Table on oi.ProductId equals product.Id
        //                          where (string.IsNullOrEmpty(productName) || product.Name.Contains(productName))
        //                          //into oij
        //                          //from orItem in oij.DefaultIfEmpty()
        //                          select new
        //                          {
        //                              ProductId = oi.ProductId,
        //                              Quantity = oi.Quantity,
        //                              CreatedOnUtc = o.CreatedOnUtc
        //                          }).AsEnumerable()
        //        .Where(
        //            o =>
        //                dateTime == null ||
        //                (o.CreatedOnUtc.Date >= dateTime.Value.Date && o.CreatedOnUtc.Date <= dateTime.Value.Date))
        //        .GroupBy(x => x.ProductId)
        //        .Select(x => new PurchaseOrderDto
        //        {
        //            ProductId = x.Key,
        //            ProductName = _productRepository.Table.FirstOrDefault(y => y.Id == x.Key).Name,
        //            Quantity = x.Sum(y => y.Quantity)
        //        }).ToList();

        //    var purchaseOrdersList = new PagedList<PurchaseOrderDto>(purchaseOrders, pageIndex, pageSize);
        //    return purchaseOrdersList;
        //}

        public IPagedList<OrderPurchaseOrderDto> GetAllPendingOrders(int workShiftId, DateTime orderDate, int pageIndex = 0,
            int pageSize = int.MaxValue, bool showHidden = false)
        {
            var workShift = _workShiftRepository.GetById(workShiftId);

            DateTime startDateTime;
            DateTime endDateTime;

            TimeSpan timeDiff = new TimeSpan(workShift.EndTime.Ticks - workShift.StartTime.Ticks);
            if (timeDiff < TimeSpan.Zero)
            {
                long numberOfTicks = timeDiff.Ticks;
                long ticksInPositive = ~numberOfTicks + 1;
                long ticksToAdd = ticksInPositive + (TimeSpan.TicksPerDay - ticksInPositive - ticksInPositive); // 24 hours in a day and subtract difference in hours twice will give actual no of hours between those hours
                startDateTime = orderDate + workShift.StartTime;
                endDateTime = startDateTime.AddTicks(ticksToAdd);
            }
            else
            {
                startDateTime = orderDate + workShift.StartTime;
                endDateTime = startDateTime.AddTicks(timeDiff.Ticks);
            }

            startDateTime = _dateTimeHelper.ConvertToUtcTime(startDateTime, DateTimeKind.Local);
            endDateTime = _dateTimeHelper.ConvertToUtcTime(endDateTime, DateTimeKind.Local);

            var pendingOrders =
                _orderRepository.Table.Where(
                    x =>
                        x.OrderStatusId == (int)OrderStatus.Pending && !x.Deleted && x.CreatedOnUtc >= startDateTime &&
                        x.CreatedOnUtc <= endDateTime && x.PurchaseOrderId == null)
                    .ToList()
                    .Select(x => new OrderPurchaseOrderDto
                    {
                        Id = x.Id,
                        FirstName = x.BillingAddress.FirstName,
                        LastName = x.BillingAddress.LastName,
                        FullName = x.BillingAddress.FirstName + " " + x.BillingAddress.LastName,
                        Address = x.BillingAddress.Address1 + " " + x.BillingAddress.Address2,
                        OrderDate = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc),
                        StringOrderDate =
                            _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                                .ToString("dd/MM/yyyy hh:mm tt")
                    }).AsQueryable();

            //var orderPurchaseOrderDto =
            //    pendingOrders.Where(
            //        x =>
            //            ).ToList();

            var orderPurchaseOrderList = new PagedList<OrderPurchaseOrderDto>(pendingOrders, pageIndex, pageSize);
            return orderPurchaseOrderList;
        }

        public List<MergedOrderItemDto> GetMergedOrderItem(List<int> orderIds)
        {
            var orderItems = (from o in _orderRepository.Table
                              join oi in _orderItemRepository.Table on o.Id equals oi.OrderId
                              join product in _productRepository.Table on oi.ProductId equals product.Id
                              where (orderIds.Count == 0 || orderIds.Contains(o.Id))
                              select new
                              {
                                  Product = product,
                                  Quantity = oi.Quantity
                              }).AsEnumerable()
                .GroupBy(x => x.Product)
                .Select(x => new MergedOrderItemDto
                {
                    Product = x.Key,
                    Quantity = x.Sum(y => y.Quantity)
                }).ToList();

            return orderItems;
        }

        public List<PlanRouteDto> GetOrdersLocations(int purchaseOrderId)
        {
            var orderLocations =
                _orderRepository.Table.Where(x => x.PurchaseOrderId == purchaseOrderId)
                    .ToList();

            return orderLocations.Select(x => new PlanRouteDto
            {
                Latitude = x.BillingAddress.Latitude,
                Longitude = x.BillingAddress.Longitude,
                CustomerName = x.BillingAddress.FirstName + " " + x.BillingAddress.LastName,
                CompleteAddress =
                            x.BillingAddress.Address1 +
                            (!string.IsNullOrWhiteSpace(x.BillingAddress.Address2) ? " " : String.Empty) +
                            x.BillingAddress.Address2 + " " + x.BillingAddress.City
            }).ToList();
        }

        #endregion

        public virtual void UpdateOrderStatusByPurchaseOrder(int purchaseOrderId)
        {
            if (_orderRepository.Table.Any(x => x.PurchaseOrderId == purchaseOrderId))
            {
                var orders = _orderRepository.Table.Where(q => q.PurchaseOrderId == purchaseOrderId).ToList();
                foreach (var order in orders)
                {
                    EngineContext.Current.Resolve<IOrderProcessingService>().DispatchOrder(order, true);
                }
            }
        }

        public virtual bool ShowDispatchOrder(int purchaseOrderId)
        {
            return _orderRepository.Table.Any(q => q.PurchaseOrderId == purchaseOrderId && q.OrderStatusId == (int)OrderStatus.Processing);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Competitors;
using Nop.Core.Domain.Directory;
using Nop.Services.Catalog;
using Nop.Services.Dto;
using Nop.Services.Events;

namespace Nop.Services.CompetitorProducts
{
    public partial class CompetitorProductService : ICompetitorProductService
    {
        #region Fields

        private readonly IRepository<Competitor> _competitorRepository;
        private readonly IRepository<CompetitorProduct> _competitorProductRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IRepository<Product> _productRepository;
        private readonly IPriceFormatter _priceFormatter;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="competitorRepository">Competitor repository</param>
        /// <param name="competitorProductRepository">Competitor product repository</param>
        /// <param name="eventPublisher">Event published</param>
        /// <param name="productRepository">Product repository</param>
        /// <param name="priceFormatter">Price Formatter</param>
        public CompetitorProductService(IRepository<Competitor> competitorRepository,
            IRepository<CompetitorProduct> competitorProductRepository,
            IEventPublisher eventPublisher, IRepository<Product> productRepository,
            IPriceFormatter priceFormatter)
        {
            this._competitorRepository = competitorRepository;
            this._competitorProductRepository = competitorProductRepository;
            this._eventPublisher = eventPublisher;
            this._productRepository = productRepository;
            this._priceFormatter = priceFormatter;
        }

        #endregion

        #region Methods

        public CompetitorProduct GetCompetitorProductById(int competitorProductId)
        {
            if (competitorProductId == 0)
                return null;

            return _competitorProductRepository.GetById(competitorProductId);
        }

        public void DeleteCompetitorProduct(CompetitorProduct competitorProduct)
        {
            if (competitorProduct == null)
                throw new ArgumentNullException("competitorProduct");

            competitorProduct.Deleted = true;
            UpdateCompetitorProduct(competitorProduct);

            //event notification
            _eventPublisher.EntityDeleted(competitorProduct);
        }

        public virtual void InsertCompetitorProduct(CompetitorProduct competitorProduct)
        {
            if (competitorProduct == null)
                throw new ArgumentNullException("competitorProduct");

            _competitorProductRepository.Insert(competitorProduct);

            //event notification
            _eventPublisher.EntityInserted(competitorProduct);
        }

        public virtual void UpdateCompetitorProduct(CompetitorProduct competitorProduct)
        {
            if (competitorProduct == null)
                throw new ArgumentNullException("competitorProduct");

            _competitorProductRepository.Update(competitorProduct);

            //event notification
            _eventPublisher.EntityUpdated(competitorProduct);
        }

        public virtual IPagedList<CompetitorProduct> GetAllCompetitorProducts(string competitorName = "",
            string productName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false)
        {
            var query = from comProd in _competitorProductRepository.Table
                        join com in _competitorRepository.Table on comProd.CompetitorId equals com.Id
                        join prod in _productRepository.Table on comProd.ProductId equals prod.Id
                        where (string.IsNullOrEmpty(competitorName) || com.Name.Contains(competitorName)) &&
                              (string.IsNullOrEmpty(productName) || prod.Name.Contains(productName)) &&
                              (showHidden || comProd.Active) && !comProd.Deleted
                        orderby comProd.DisplayOrder
                        select comProd;

            var competitorProducts = new PagedList<CompetitorProduct>(query, pageIndex, pageSize);
            return competitorProducts;
        }

        public int GetCompetitorProductAssociatedCompetitorCount(int competitorId)
        {
            if (competitorId == 0)
                return 0;

            return _competitorProductRepository.Table.Where(x => x.CompetitorId == competitorId).Count();
        }

        public List<CompetitorProductDto> GetCompetitorProductByProductId(int productId)
        {
            if (productId == 0)
                return null;

            var products = _competitorProductRepository.Table.Where(x => x.ProductId == productId && x.Active).ToList();
            List<CompetitorProductDto> result = products.Select(x => new CompetitorProductDto
            {
                Id = x.Id,
                ProductId = x.ProductId,
                CompetitorId = x.CompetitorId,
                Price = x.Price,
                PriceString = _priceFormatter.FormatPrice(x.Price, true, false),
                ProductName = x.Product.Name,
                CompetitorName = x.Competitor.Name,
                Active = x.Active,
                DisplayOrder = x.DisplayOrder
            }).ToList();


            return result;
        }

        public virtual void InsertOrUpdateCompetitorProduct(CompetitorProduct competitorProduct)
        {
            if (competitorProduct == null)
                throw new ArgumentNullException("competitorProduct");

            CompetitorProduct comProduct =
                _competitorProductRepository.Table.FirstOrDefault(
                    x => x.ProductId == competitorProduct.ProductId && x.CompetitorId == competitorProduct.CompetitorId);

            if (comProduct == null)
            {
                _competitorProductRepository.Insert(competitorProduct);

                //event notification
                _eventPublisher.EntityInserted(competitorProduct);
            }
            else
            {
                comProduct.Price = competitorProduct.Price;
                _competitorProductRepository.Update(comProduct);
                
                //event notification
                _eventPublisher.EntityUpdated(competitorProduct);
            }
        }

        #endregion
    }
}
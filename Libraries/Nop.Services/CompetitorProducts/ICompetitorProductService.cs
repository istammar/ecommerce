﻿using System.Collections.Generic;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Competitors;
using Nop.Services.Dto;

namespace Nop.Services.CompetitorProducts
{
    public partial interface ICompetitorProductService
    {
        /// <summary>
        /// Gets a competitor product by competitor identifier
        /// </summary>
        /// <param name="competitorProductId">Competitor Product identifier</param>
        /// <returns>Competitor Product</returns>
        CompetitorProduct GetCompetitorProductById(int competitorProductId);

        /// <summary>
        /// Delete a competitor product
        /// </summary>
        /// <param name="competitorProduct">Competitor Product</param>
        void DeleteCompetitorProduct(CompetitorProduct competitorProduct);

        /// <summary>
        /// Inserts a competitor product
        /// </summary>
        /// <param name="competitorProduct">Competitor Product</param>
        void InsertCompetitorProduct(CompetitorProduct competitorProduct);

        /// <summary>
        /// Updates the competitor product
        /// </summary>
        /// <param name="competitorProduct">Competitor Product</param>
        void UpdateCompetitorProduct(CompetitorProduct competitorProduct);

        /// <summary>
        /// Gets all competitor products
        /// </summary>
        /// <param name="competitorId">Competitor Id</param>
        /// <param name="productId">Product Id</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>Competitor Product</returns>
        IPagedList<CompetitorProduct> GetAllCompetitorProducts(string competitorName = "",
            string productName = "", int pageIndex = 0, int pageSize = int.MaxValue, bool showHidden = false);

        /// <summary>
        /// Gets a competitor product by competitor id identifier
        /// </summary>
        /// <param name="competitorId">Competitor id identifier</param>
        /// <returns>Competitor Product</returns>
        int GetCompetitorProductAssociatedCompetitorCount(int competitorId);

        /// <summary>
        /// Gets a competitor product by product id identifier
        /// </summary>
        /// <param name="productId">Product id identifier</param>
        /// <returns>Competitor Product List</returns>
        List<CompetitorProductDto> GetCompetitorProductByProductId(int productId);


        void InsertOrUpdateCompetitorProduct(CompetitorProduct competitorProduct);
    }
}
﻿namespace Nop.Services.Dto
{
    public class PlanRouteDto
    {
        /// <summary>
        /// Get or set the customer name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Gets or sets the Latitude
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Gets or sets the Longitude
        /// </summary>
        public string Longitude { get; set; }

        public string CompleteAddress { get; set; }
    }
}

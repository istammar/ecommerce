﻿using System;
using System.Collections.Generic;

namespace Nop.Services.Dto
{
    public partial class ShoppingEditListDto
    {
        public ShoppingEditListDto()
        {
            Warnings = new List<string>();
        }

        public Boolean Delete { get; set; }

        public int Id { get; set; }

        public int ProductId { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }

        public string PriceString { get; set; }

        public int Quantity { get; set; }
        
        public decimal Total { get; set; }

        public string TotalString { get; set; }

        public IList<string> Warnings { get; set; }

    }
}

﻿using Nop.Core.Domain.Catalog;

namespace Nop.Services.Dto
{
    public class MergedOrderItemDto
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }
    }
}

﻿using System;

namespace Nop.Services.Dto
{
    public partial class OrderPurchaseOrderDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the customer first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the customer last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the customer full name
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the customer Address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets the date and time of order creation
        /// </summary>
        public DateTime OrderDate { get; set; }

        public string StringOrderDate { get; set; }
    }
}

﻿using Nop.Core.Domain.Catalog;

namespace Nop.Services.Dto
{
    public class PurchaseOrderDto
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int Quantity { get; set; }
    }
}

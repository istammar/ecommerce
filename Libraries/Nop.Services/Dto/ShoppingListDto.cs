﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Services.Dto
{
    public partial class ShoppingListDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int ProductQuantity { get; set; }
    }
}

﻿using Nop.Core.Domain.Catalog;

namespace Nop.Core.Domain.PurchaseOrders
{
    public partial class PurchaseOrderItem : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Purchase Order Id
        /// </summary>
        public int PurchaseOrderId { get; set; }

        /// <summary>
        /// Gets or sets the Product id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the Product SKU
        /// </summary>
        public string ProductSku { get; set; }

        /// <summary>
        /// Gets or sets the Product Name
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Gets or sets the Product Quantity
        /// </summary>
        public int ProductQuantity { get; set; }

        /// <summary>
        /// Gets or sets the Product Vendor
        /// </summary>
        public int? VendorId { get; set; }

        /// <summary>
        /// Gets the Purchase Order
        /// </summary>
        public virtual PurchaseOrder PurchaseOrder { get; set; }

        /// <summary>
        /// Gets the Product
        /// </summary>
        public virtual Product Product { get; set; }
    }
}

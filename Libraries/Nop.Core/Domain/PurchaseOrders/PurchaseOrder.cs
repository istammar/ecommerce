﻿using System;
using System.Collections.Generic;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Seo;

namespace Nop.Core.Domain.PurchaseOrders
{
    public partial class PurchaseOrder : BaseEntity, ILocalizedEntity, ISlugSupported
    {
        private ICollection<PurchaseOrderItem> _purchaseOrderItems;

        /// <summary>
        /// Gets or sets the desciption
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the desciption
        /// </summary>
        public int TotalQuantity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the date and time of product creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }
        /// <summary>
        /// Gets or sets the date and time of product update
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets competitor products
        /// </summary>
        public virtual ICollection<PurchaseOrderItem> PurchaseOrderItems
        {
            get { return _purchaseOrderItems ?? (_purchaseOrderItems = new List<PurchaseOrderItem>()); }
            protected set { _purchaseOrderItems = value; }
        }
    }
}

﻿using System;

namespace Nop.Core.Domain.Customers
{
    public partial class ShoppingCheckListItem : BaseEntity
    {
        public int ShoppingCheckListId { get; set; }

        public string ProductType { get; set; }

        public int Quantity { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public virtual ShoppingCheckList ShoppingCheckList { get; set; }
    }
}

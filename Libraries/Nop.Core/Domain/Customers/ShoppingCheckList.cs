﻿using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Customers
{
    public partial class ShoppingCheckList : BaseEntity
    {
        private ICollection<ShoppingCheckListItem> _shoppingCheckListItems;

        public string Name { get; set; }

        public int CustomerId { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public virtual ICollection<ShoppingCheckListItem> ShoppingCheckListItems
        {
            get { return _shoppingCheckListItems ?? (_shoppingCheckListItems = new List<ShoppingCheckListItem>()); }
            set { _shoppingCheckListItems = value; }
        }
    }
}

﻿namespace Nop.Core.Domain.Customers
{
    public partial class ShoppingListItem : BaseEntity
    {
        public int ShoppingListId { get; set; }

        public int ProductId { get; set; }

        public int Quantity { get; set; }

        public virtual ShoppingList ShoppingList { get; set; }
    }
}

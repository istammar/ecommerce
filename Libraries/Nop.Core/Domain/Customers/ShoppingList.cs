﻿using System;
using System.Collections.Generic;

namespace Nop.Core.Domain.Customers
{
    public partial class ShoppingList : BaseEntity
    {
        private ICollection<ShoppingListItem> _shoppingListItems;

        public string Name { get; set; }

        public int CustomerId { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }
        
        public virtual ICollection<ShoppingListItem> ShoppingListItems
        {
            get { return _shoppingListItems ?? (_shoppingListItems = new List<ShoppingListItem>()); }
            set { _shoppingListItems = value; }
        }
    }
}

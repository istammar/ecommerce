﻿using System;
using Nop.Core.Domain.Stores;

namespace Nop.Core.Domain.WorkShifts
{
    public class WorkShift : BaseEntity
    {
        /// <summary>
        /// Gets or sets the Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the Start Time
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// Gets or sets the End Time
        /// </summary>
        public TimeSpan EndTime { get; set; }

        /// <summary>
        /// Gets or sets the Store Id
        /// </summary>
        public int StoreId { get; set; }

        /// <summary>
        /// Gets or sets the Published
        /// </summary>
        public bool Published { get; set; }

        /// <summary>
        /// Gets or sets the CreatedOnUtc
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the UpdatedOnUtc
        /// </summary>
        public DateTime UpdatedOnUtc { get; set; }

        public Store Store { get; set; }
    }
}

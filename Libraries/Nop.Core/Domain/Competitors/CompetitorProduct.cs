﻿using Nop.Core.Domain.Catalog;

namespace Nop.Core.Domain.Competitors
{
    public partial class CompetitorProduct : BaseEntity
    {
        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Gets or sets the competitor id
        /// </summary>
        public int CompetitorId { get; set; }

        /// <summary>
        /// Gets or sets the price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the entity has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int? DisplayOrder { get; set; }

        /// <summary>
        /// Gets or sets the page size
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether customers can select the page size
        /// </summary>
        public bool AllowCustomersToSelectPageSize { get; set; }

        /// <summary>
        /// Gets or sets the available customer selectable page size options
        /// </summary>
        public string PageSizeOptions { get; set; }

        /// <summary>
        /// Gets the competitor
        /// </summary>
        public virtual Competitor Competitor { get; set; }
        
        /// <summary>
        /// Gets the product
        /// </summary>
        public virtual Product Product { get; set; }
    }
}

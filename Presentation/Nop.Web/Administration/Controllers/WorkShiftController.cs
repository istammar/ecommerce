﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Competitors;
using Nop.Admin.Models.WorkShifts;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.WorkShifts;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Stores;
using Nop.Services.WorkShifts;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Admin.Controllers
{
    public class WorkShiftController : BaseAdminController
    {
        #region Fields

        private readonly IWorkShiftService _workShiftService;
        private readonly IStoreService _storeService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ILanguageService _languageService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILocalizedEntityService _localizedEntityService;

        #endregion Fields

        #region Constructors

        public WorkShiftController(IWorkShiftService workShiftService, IPermissionService permissionService,
            ILanguageService languageService, ILocalizationService localizationService,
            ICustomerActivityService customerActivityService, IUrlRecordService urlRecordService,
            ILocalizedEntityService localizedEntityService, IStoreService storeService)
        {
            this._workShiftService = workShiftService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._languageService = languageService;
            this._customerActivityService = customerActivityService;
            this._urlRecordService = urlRecordService;
            this._localizedEntityService = localizedEntityService;
            _storeService = storeService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareWorkShiftModel(WorkShiftModel model)
        {
            // get active competitors
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "" });
            foreach (var competitor in _storeService.GetAllStores())
            {
                model.AvailableStores.Add(new SelectListItem
                {
                    Value = competitor.Id.ToString(),
                    Text = competitor.Name
                });
            }
        }

        #endregion

        #region Methods

        // GET: Work Shifts
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // List
        public virtual ActionResult List()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
            //    return AccessDeniedView();

            var model = new WorkShiftListModel();
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult List(DataSourceRequest command, WorkShiftListModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
            //    return AccessDeniedKendoGridJson();

            var workShifts = _workShiftService.GetAllWorkShifts(model.SearchName, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = workShifts.Select(x =>
                {
                    var workShiftModel = x.ToModel();
                    return workShiftModel;
                }),
                Total = workShifts.TotalCount
            };

            return Json(gridModel);
        }

        // Create
        public virtual ActionResult Create()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();


            var model = new WorkShiftModel();
            PrepareWorkShiftModel(model);

            //default values
            model.Published = true;
            
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual ActionResult Create(WorkShiftModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            ModelState.Remove("StartTime");
            ModelState.Remove("EndTime");

            if (ModelState.IsValid)
            {
                model.CreatedOnUtc = DateTime.UtcNow;
                model.UpdatedOnUtc = DateTime.UtcNow;
                var competitor = model.ToEntity();
                _workShiftService.InsertWorkShift(competitor);

                //activity log
                _customerActivityService.InsertActivity("AddNewWorkShift", _localizationService.GetResource("ActivityLog.AddNewWorkShift"), competitor.Id);
                
                SuccessNotification(_localizationService.GetResource("Admin.WorkShifts.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = competitor.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareWorkShiftModel(model);

            return View(model);
        }

        // Edit
        public virtual ActionResult Edit(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var workShift = _workShiftService.GetWorkShiftById(id);

            var model = workShift.ToModel();
            PrepareWorkShiftModel(model);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual ActionResult Edit(WorkShiftModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            WorkShift workShift = _workShiftService.GetWorkShiftById(model.Id);

            ModelState.Remove("StartTime");
            ModelState.Remove("EndTime");

            if (ModelState.IsValid)
            {
                workShift = model.ToEntity(workShift);
                _workShiftService.UpdateWorkShift(workShift);

                //activity log
                _customerActivityService.InsertActivity("EditWorkShift", _localizationService.GetResource("ActivityLog.EditWorkShift"), workShift.Id);

                SuccessNotification(_localizationService.GetResource("Admin.WorkShifts.Updated"));
                if (continueEditing)
                {
                    return RedirectToAction("Edit", new { id = workShift.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareWorkShiftModel(model);

            return View(model);
        }

        #endregion
    }
}
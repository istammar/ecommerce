﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ExcelDataReader;
using Nop.Admin.Extensions;
using Nop.Admin.Models.CompetitorProducts;
using Nop.Core;
using Nop.Core.Domain.Competitors;
using Nop.Core.Domain.Vendors;
using Nop.Services.CompetitorProducts;
using Nop.Services.Competitors;
using Nop.Services.ExportImport;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Admin.Controllers
{
    public class CompetitorProductController : BaseAdminController
    {
        #region Fields

        private readonly ICompetitorService _competitorService;
        private readonly ICompetitorProductService _competitorProductService;
        private readonly IPermissionService _permissionService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly VendorSettings _vendorSettings;
        private readonly IImportManager _importManager;

        #endregion Fields

        #region Constructors

        public CompetitorProductController(ICompetitorProductService competitorProductService, IPermissionService permissionService,
            ILanguageService languageService, ILocalizationService localizationService, ICompetitorService competitorService,
            ICustomerActivityService customerActivityService, IUrlRecordService urlRecordService, IWorkContext workContext, VendorSettings vendorSettings, IImportManager importManager)
        {
            this._competitorService = competitorService;
            this._competitorProductService = competitorProductService;
            this._permissionService = permissionService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._customerActivityService = customerActivityService;
            this._urlRecordService = urlRecordService;
            this._workContext = workContext;
            this._vendorSettings = vendorSettings;
            _importManager = importManager;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareCompetitorProductModel(CompetitorProductModel model)
        {
            // get active competitors
            model.AvailableCompetitors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.Select"), Value = "" });
            foreach (var competitor in _competitorService.GetAllCompetitorList())
            {
                model.AvailableCompetitors.Add(new SelectListItem
                {
                    Value = competitor.Id.ToString(),
                    Text = competitor.Name
                });
            }
        }


        #endregion

        #region CompetitorProducts

        public virtual ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual ActionResult List()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var model = new CompetitorProductListModel();
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult List(DataSourceRequest command, CompetitorProductListModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedKendoGridJson();

            var competitorProducts = _competitorProductService.GetAllCompetitorProducts(model.CompetitorName, model.ProductName,
                command.Page - 1, command.PageSize, true);

            var gridModel = new DataSourceResult
            {
                Data = competitorProducts.Select(x =>
                {
                    var competitorProductModel = x.ToModel();
                    return competitorProductModel;
                }),
                Total = competitorProducts.TotalCount,
            };

            return Json(gridModel);
        }

        // create
        public virtual ActionResult Create()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var model = new CompetitorProductModel();
            PrepareCompetitorProductModel(model);
            //locales
            AddLocales(_languageService, model.Locales);
            //default values
            model.PageSize = 10;
            model.Active = true;
            model.AllowCustomersToSelectPageSize = true;
            model.PageSizeOptions = "10, 5, 15, 20";

            //default value
            model.Active = true;

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual ActionResult Create(CompetitorProductModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var competitorProduct = model.ToEntity();
                _competitorProductService.InsertCompetitorProduct(competitorProduct);

                //activity log
                _customerActivityService.InsertActivity("AddNewCompetitorProduct", _localizationService.GetResource("ActivityLog.AddNewCompetitorProduct"), competitorProduct.Id);

                SuccessNotification(_localizationService.GetResource("Admin.CompetitorProducts.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = competitorProduct.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareCompetitorProductModel(model);

            return View(model);
        }

        //edit
        public virtual ActionResult Edit(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var competitor = _competitorProductService.GetCompetitorProductById(id);
            if (competitor == null || competitor.Deleted)
                //No competitor found with the specified id
                return RedirectToAction("List");

            var model = competitor.ToModel();
            PrepareCompetitorProductModel(model);

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual ActionResult Edit(CompetitorProductModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            CompetitorProduct competitorProduct = _competitorProductService.GetCompetitorProductById(model.Id);
            if (competitorProduct == null || competitorProduct.Deleted)
                //No competitor found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                competitorProduct = model.ToEntity(competitorProduct);
                _competitorProductService.UpdateCompetitorProduct(competitorProduct);

                //activity log
                _customerActivityService.InsertActivity("EditCompetitorProduct", _localizationService.GetResource("ActivityLog.EditCompetitorProduct"), competitorProduct.Id);

                SuccessNotification(_localizationService.GetResource("Admin.CompetitorProductss.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = competitorProduct.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareCompetitorProductModel(model);

            return View(model);
        }

        //delete
        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var competitor = _competitorProductService.GetCompetitorProductById(id);
            if (competitor == null)
                //No competitor found with the specified id
                return RedirectToAction("List");

            //delete a competitor
            _competitorProductService.DeleteCompetitorProduct(competitor);

            //activity log
            _customerActivityService.InsertActivity("DeleteCompetitorProduct", _localizationService.GetResource("ActivityLog.DeleteCompetitorProduct"), competitor.Id);

            SuccessNotification(_localizationService.GetResource("Admin.CompetitorProducts.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

        #region Competitor Price Import

        [HttpPost]
        public virtual ActionResult ImportExcel()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (_workContext.CurrentVendor != null && !_vendorSettings.AllowVendorsToImportProducts)
                //a vendor can not import products
                return AccessDeniedView();

            try
            {
                var file = Request.Files["importexcelfile"];
                if (file != null && file.ContentLength > 0)
                {
                    IExcelDataReader excelReader = default(IExcelDataReader);
                    var dataSet = new Dictionary<string, DataRowCollection>();
                    Stream stream = file.InputStream;
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    DataSet excelDataSet = excelReader.AsDataSet();
                    foreach (DataTable table in excelDataSet.Tables)
                    {
                        dataSet.Add(table.TableName, table.Rows);
                    }
                    Dictionary<string, string> result = _importManager.ImportCompetitorPricesFromXlsx(dataSet);
                    if (result["status"] == "success")
                    {
                        SuccessNotification(result["message"]);
                    }
                    else
                    {
                        ErrorNotification(result["message"]);
                    }
                    return RedirectToAction("List");
                }
                else
                {
                    ErrorNotification(_localizationService.GetResource("Admin.Common.UploadFile"));
                    return RedirectToAction("List");
                }
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }

        }

        #endregion
    }
}
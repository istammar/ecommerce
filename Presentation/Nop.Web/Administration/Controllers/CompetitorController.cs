﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models.Competitors;
using Nop.Core.Domain.Competitors;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Localization;
using Nop.Services.Common;
using Nop.Services.CompetitorProducts;
using Nop.Services.Competitors;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;

namespace Nop.Admin.Controllers
{
    public class CompetitorController : BaseAdminController
    {
        #region Fields

        private readonly ICompetitorService _competitorService;
        //private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly ILanguageService _languageService;
        private readonly ICompetitorProductService _competitorProductService;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly IPictureService _pictureService;

        #endregion Fields

        #region Constructors

        public CompetitorController(ICompetitorService competitorService, IPermissionService permissionService,
            ILanguageService languageService, ILocalizationService localizationService, ICompetitorProductService competitorProductService,
            ICustomerActivityService customerActivityService, IAddressService addressService, IUrlRecordService urlRecordService,
            /*IDateTimeHelper dateTimeHelper,*/ ICountryService countryService, IStateProvinceService stateProvinceService,
            ILocalizedEntityService localizedEntityService, IPictureService pictureService)
        {
            this._competitorService = competitorService;
            //this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._addressService = addressService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._languageService = languageService;
            this._competitorProductService = competitorProductService;
            this._customerActivityService = customerActivityService;
            this._urlRecordService = urlRecordService;
            this._localizedEntityService = localizedEntityService;
            this._pictureService = pictureService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void UpdatePictureSeoNames(Competitor competitor)
        {
            var picture = _pictureService.GetPictureById(Convert.ToInt32(competitor.PictureId));
            if (picture != null)
                _pictureService.SetSeoFilename(picture.Id, _pictureService.GetPictureSeName(competitor.Name));
        }

        [NonAction]
        protected virtual void UpdateLocales(Competitor competitor, CompetitorModel model)
        {
            foreach (var localized in model.Locales)
            {
                _localizedEntityService.SaveLocalizedValue(competitor,
                                                               x => x.Name,
                                                               localized.Name,
                                                               localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(competitor,
                                                           x => x.Description,
                                                           localized.Description,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(competitor,
                                                           x => x.MetaKeywords,
                                                           localized.MetaKeywords,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(competitor,
                                                           x => x.MetaDescription,
                                                           localized.MetaDescription,
                                                           localized.LanguageId);

                _localizedEntityService.SaveLocalizedValue(competitor,
                                                           x => x.MetaTitle,
                                                           localized.MetaTitle,
                                                           localized.LanguageId);

                //search engine name
                var seName = competitor.ValidateSeName(localized.SeName, localized.Name, false);
                _urlRecordService.SaveSlug(competitor, seName, localized.LanguageId);
            }
        }

        [NonAction]
        protected virtual void PrepareCompetitorModel(CompetitorModel model, Competitor competitor, bool excludeProperties,
            bool prepareEntireAddressModel)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var address = _addressService.GetAddressById(competitor != null ? Convert.ToInt32(competitor.AddressId) : 0);

            if (competitor != null)
            {
                if (!excludeProperties)
                {
                    if (address != null)
                    {
                        model.Address = address.ToModel();
                    }
                }
            }

            if (prepareEntireAddressModel)
            {
                model.Address.CountryEnabled = true;
                model.Address.StateProvinceEnabled = true;
                model.Address.CityEnabled = true;
                model.Address.StreetAddressEnabled = true;
                model.Address.StreetAddress2Enabled = true;
                model.Address.ZipPostalCodeEnabled = true;
                model.Address.PhoneEnabled = true;
                model.Address.FaxEnabled = true;

                //address
                model.Address.AvailableCountries.Add(new SelectListItem
                {
                    Text = _localizationService.GetResource("Admin.Address.SelectCountry"),
                    Value = "0"
                });
                foreach (var c in _countryService.GetAllCountries(showHidden: true))
                    model.Address.AvailableCountries.Add(new SelectListItem
                    {
                        Text = c.Name,
                        Value = c.Id.ToString(),
                        Selected = (address != null && c.Id == address.CountryId)
                    });

                var states = model.Address.CountryId.HasValue
                    ? _stateProvinceService.GetStateProvincesByCountryId(model.Address.CountryId.Value, showHidden: true)
                        .ToList()
                    : new List<StateProvince>();
                if (states.Any())
                {
                    foreach (var s in states)
                        model.Address.AvailableStates.Add(new SelectListItem
                        {
                            Text = s.Name,
                            Value = s.Id.ToString(),
                            Selected = (address != null && s.Id == address.StateProvinceId)
                        });
                }
                else
                {
                    model.Address.AvailableStates.Add(new SelectListItem
                    {
                        Text = _localizationService.GetResource("Admin.Address.OtherNonUS"),
                        Value = "0"
                    });
                }
            }
        }

        #endregion

        #region Competitors

        // list
        public virtual ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public virtual ActionResult List()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var model = new CompetitorListModel();
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult List(DataSourceRequest command, CompetitorListModel model)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedKendoGridJson();

            var competitors = _competitorService.GetAllCompetitors(model.SearchName, command.Page - 1, command.PageSize,
                true);
            var gridModel = new DataSourceResult
            {
                Data = competitors.Select(x =>
                {
                    var competitorModel = x.ToModel();
                    return competitorModel;
                }),
                Total = competitors.TotalCount,
            };

            return Json(gridModel);
        }

        // create
        public virtual ActionResult Create()
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();


            var model = new CompetitorModel();
            PrepareCompetitorModel(model, null, false, true);
            //locales
            AddLocales(_languageService, model.Locales);
            //default values
            model.PageSize = 10;
            model.Active = true;
            model.AllowCustomersToSelectPageSize = true;
            model.PageSizeOptions = "10, 5, 15, 20";

            //default value
            model.Active = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public virtual ActionResult Create(CompetitorModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var competitor = model.ToEntity();
                _competitorService.InsertCompetitor(competitor);

                //activity log
                _customerActivityService.InsertActivity("AddNewCompetitor", _localizationService.GetResource("ActivityLog.AddNewCompetitor"), competitor.Id);

                //search engine name
                model.SeName = competitor.ValidateSeName(model.SeName, competitor.Name, true);
                IList<Language> languages = _languageService.GetAllLanguages(true);
                _urlRecordService.SaveSlug(competitor, model.SeName, languages.FirstOrDefault().Id);

                //address
                var address = model.Address.ToEntity();
                address.CreatedOnUtc = DateTime.UtcNow;
                //some validation
                if (address.CountryId == 0)
                    address.CountryId = null;
                if (address.StateProvinceId == 0)
                    address.StateProvinceId = null;
                _addressService.InsertAddress(address);
                competitor.AddressId = address.Id;
                _competitorService.UpdateCompetitor(competitor);

                //locales
                UpdateLocales(competitor, model);
                //update picture seo file name
                if (competitor.PictureId != null)
                {
                    UpdatePictureSeoNames(competitor);
                }

                SuccessNotification(_localizationService.GetResource("Admin.Competitors.Added"));

                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = competitor.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareCompetitorModel(model, null, true, true);

            return View(model);
        }

        //edit
        public virtual ActionResult Edit(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var competitor = _competitorService.GetCompetitorById(id);
            if (competitor == null || competitor.Deleted)
                //No competitor found with the specified id
                return RedirectToAction("List");

            var model = competitor.ToModel();
            IList<Language> languages = _languageService.GetAllLanguages(true);
            model.SeName = competitor.GetSeName(languages.FirstOrDefault().Id, false, false);
            PrepareCompetitorModel(model, competitor, false, true);

            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Name = competitor.GetLocalized(x => x.Name, languageId, false, false);
                locale.Description = competitor.GetLocalized(x => x.Description, languageId, false, false);
                locale.MetaKeywords = competitor.GetLocalized(x => x.MetaKeywords, languageId, false, false);
                locale.MetaDescription = competitor.GetLocalized(x => x.MetaDescription, languageId, false, false);
                locale.MetaTitle = competitor.GetLocalized(x => x.MetaTitle, languageId, false, false);
                locale.SeName = competitor.GetSeName(languageId, false, false);
            });

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual ActionResult Edit(CompetitorModel model, bool continueEditing)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            Competitor competitor = _competitorService.GetCompetitorById(model.Id);
            if (competitor == null || competitor.Deleted)
                //No competitor found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                int prevPictureId = Convert.ToInt32(competitor.PictureId);
                competitor = model.ToEntity(competitor);
                _competitorService.UpdateCompetitor(competitor);

                //activity log
                _customerActivityService.InsertActivity("EditCompetitor", _localizationService.GetResource("ActivityLog.EditCompetitor"), competitor.Id);

                //search engine name
                model.SeName = competitor.ValidateSeName(model.SeName, competitor.Name, true);
                IList<Language> languages = _languageService.GetAllLanguages(true);
                _urlRecordService.SaveSlug(competitor, model.SeName, languages.FirstOrDefault().Id);

                //address
                var address = _addressService.GetAddressById(Convert.ToInt32(competitor.AddressId));
                if (address == null)
                {
                    address = model.Address.ToEntity();
                    address.CreatedOnUtc = DateTime.UtcNow;
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.InsertAddress(address);

                    competitor.AddressId = address.Id;
                    _competitorService.UpdateCompetitor(competitor);
                }
                else
                {
                    address = model.Address.ToEntity(address);
                    //some validation
                    if (address.CountryId == 0)
                        address.CountryId = null;
                    if (address.StateProvinceId == 0)
                        address.StateProvinceId = null;

                    _addressService.UpdateAddress(address);
                }


                //locales
                UpdateLocales(competitor, model);
                if (competitor.PictureId > 0)
                {
                    //delete an old picture (if deleted or updated)
                    if (prevPictureId > 0 && prevPictureId != competitor.PictureId)
                    {
                        var prevPicture = _pictureService.GetPictureById(prevPictureId);
                        if (prevPicture != null)
                            _pictureService.DeletePicture(prevPicture);
                    }
                    //update picture seo file name
                    UpdatePictureSeoNames(competitor);
                }

                SuccessNotification(_localizationService.GetResource("Admin.Competitors.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabName();

                    return RedirectToAction("Edit", new { id = competitor.Id });
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareCompetitorModel(model, competitor, true, true);

            return View(model);
        }

        //delete
        [HttpPost]
        public virtual ActionResult Delete(int id)
        {
            //if (!_permissionService.Authorize(StandardPermissionProvider.ManageVendors))
            //    return AccessDeniedView();

            var competitor = _competitorService.GetCompetitorById(id);
            if (competitor == null)
                //No competitor found with the specified id
                return RedirectToAction("List");

            // check for associated entities
            int associatedCompetitorsCount = _competitorProductService.GetCompetitorProductAssociatedCompetitorCount(id);
            if (associatedCompetitorsCount > 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Competitors.Associated"));
                return RedirectToAction("List");
            }

            //delete a competitor
            _competitorService.DeleteCompetitor(competitor);

            ////activity log
            _customerActivityService.InsertActivity("DeleteCompetitor", _localizationService.GetResource("ActivityLog.DeleteCompetitor"), competitor.Id);

            SuccessNotification(_localizationService.GetResource("Admin.Competitors.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

    }
}
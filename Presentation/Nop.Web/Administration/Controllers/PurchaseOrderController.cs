﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Nop.Admin.Extensions;
using Nop.Admin.Models;
using Nop.Admin.Models.PurchaseOrders;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.PurchaseOrders;
using Nop.Services.Dto;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.PurchaseOrders;
using Nop.Services.Security;
using Nop.Services.Vendors;
using Nop.Services.WorkShifts;
using Nop.Web.Framework.Kendoui;

namespace Nop.Admin.Controllers
{
    public class PurchaseOrderController : BaseAdminController
    {
        #region Fields

        private readonly IPurchaseOrderService _purchaseOrderService;
        private readonly IOrderService _orderService;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly ILanguageService _languageService;
        private readonly IWorkShiftService _workShiftService;
        private readonly IVendorService _vendorService;

        #endregion Fields

        #region Constructors

        public PurchaseOrderController(IPurchaseOrderService purchaseOrderService,
            ILocalizationService localizationService, IPermissionService permissionService,
            ILanguageService languageService, IOrderService orderService, IWorkShiftService workShiftService, IVendorService vendorService)
        {
            this._purchaseOrderService = purchaseOrderService;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._languageService = languageService;
            this._orderService = orderService;
            _workShiftService = workShiftService;
            _vendorService = vendorService;
        }

        #endregion

        #region PurchaseOrders

        public ActionResult List()
        {
            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            var purchaseOrders = _purchaseOrderService.GetPurchaseOrderList(command.Page - 1, command.PageSize, true);

            var gridModel = new DataSourceResult
            {
                Data = purchaseOrders.Select(x =>
                {
                    var purchaseOrderModel = x.ToModel();
                    return purchaseOrderModel;
                }),
                Total = purchaseOrders.TotalCount
            };

            return Json(gridModel);
        }

        public ActionResult PendingOrders()
        {
            PendingOrderListModel model = new PendingOrderListModel();
            var workShifts = _workShiftService.GetAllWorkShifts();
            for (int i = 0; i < workShifts.Count; i++)
            {
                model.WorkShiftList.Add(new SelectListItem
                {
                    Value = workShifts[i].Id.ToString(),
                    Text = workShifts[i].Name,
                    Selected = (i == 0)
                });
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult PendingOrders(DataSourceRequest command, PendingOrderListModel model)
        {
            IPagedList<OrderPurchaseOrderDto> pendingOrders = _orderService.GetAllPendingOrders(model.WorkShift, model.OrderDate, command.Page - 1,
                command.PageSize, true);

            var gridModel = new DataSourceResult
            {
                Data = pendingOrders,
                Total = pendingOrders.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        public virtual ActionResult Consolidate(ICollection<int> selectedIds)
        {
            TempData["OrderIds"] = selectedIds;
            return Json(new { Result = true });
        }

        public ActionResult Detail(int? id)
        {
            List<PurchaseOrderItemModel> list = new List<PurchaseOrderItemModel>();
            if (id != null)
            {
                ViewBag.PurchaseOrderId = id;
                // get purchase order detail
                PurchaseOrder purchaseOrder = _purchaseOrderService.GetPurchaseOrderById(id.Value);
                list = purchaseOrder.PurchaseOrderItems.Select(x => new PurchaseOrderItemModel
                {
                    ProductId = x.ProductId,
                    ProductName = x.ProductName,
                    ProductSku = x.ProductSku,
                    ProductQuantity = x.ProductQuantity,
                    VendorName = x.Product.VendorId > 0 ? _vendorService.GetVendorById(x.Product.VendorId).Name : String.Empty
                }).ToList();
            }
            else if (TempData["OrderIds"] != null)
            {
                // consolidate purchase order
                ICollection<int> orderIds = (ICollection<int>)TempData["OrderIds"];
                TempData.Keep("OrderIds");
                List<MergedOrderItemDto> mergedOrderItems = _orderService.GetMergedOrderItem(orderIds.ToList());
                list = mergedOrderItems.Select(x => new PurchaseOrderItemModel
                {
                    ProductId = x.Product.Id,
                    ProductName = x.Product.Name,
                    ProductSku = x.Product.Sku,
                    ProductQuantity = x.Quantity,
                    VendorName = x.Product.VendorId > 0 ? _vendorService.GetVendorById(x.Product.VendorId).Name : String.Empty
                }).ToList();
            }
            return View(list);
        }

        public ActionResult GeneratePurchaseOrder()
        {
            ICollection<int> orderIds = (ICollection<int>)TempData["OrderIds"];
            _purchaseOrderService.GeneratePurchaseOrder(orderIds.ToList());
            SuccessNotification(_localizationService.GetResource("Admin.PurchaseOrders.Added"));
            return RedirectToAction("List");
        }

        public ActionResult RoutePlanning()
        {

            return View();
        }

        #endregion
    }
}
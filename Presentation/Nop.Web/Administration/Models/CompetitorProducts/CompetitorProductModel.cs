﻿using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.CompetitorProducts;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.CompetitorProducts
{
    [Validator(typeof(CompetitorProductValidator))]
    public partial class CompetitorProductModel : BaseNopEntityModel, ILocalizedModel<CompetitorProductLocalizedModel>
    {
        public CompetitorProductModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }

            Locales = new List<CompetitorProductLocalizedModel>();
            AvailableCompetitors = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.Product")]
        [AllowHtml]
        public int ProductId { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.Competitor")]
        [AllowHtml]
        public int CompetitorId { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.Price")]
        [AllowHtml]
        public decimal Price { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.Active")]
        public bool Active { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.PageSize")]
        public int PageSize { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.AllowCustomersToSelectPageSize")]
        public bool AllowCustomersToSelectPageSize { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        public IList<CompetitorProductLocalizedModel> Locales { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.List.ProductName")]
        [AllowHtml]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.List.CompetitorName")]
        [AllowHtml]
        public string CompetitorName { get; set; }

        public IList<SelectListItem> AvailableCompetitors { get; set; }

        public string PriceString { get; set; }

        [AllowHtml]
        public string ProductSku { get; set; }
    }

    public partial class CompetitorProductLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }
    }
}
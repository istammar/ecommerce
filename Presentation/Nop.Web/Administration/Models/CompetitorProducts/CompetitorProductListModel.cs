﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.CompetitorProducts
{
    public partial class CompetitorProductListModel : BaseNopModel
    {
        [NopResourceDisplayName("Admin.CompetitorProducts.List.CompetitorName")]
        [AllowHtml]
        public string CompetitorName { get; set; }

        [NopResourceDisplayName("Admin.CompetitorProducts.List.ProductName")]
        [AllowHtml]
        public string ProductName { get; set; }
    }
}
﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.WorkShifts
{
    public partial class WorkShiftListModel : BaseNopModel
    {
        [NopResourceDisplayName("Admin.WorkShifts.List.SearchName")]
        [AllowHtml]
        public string SearchName { get; set; }
    }
}
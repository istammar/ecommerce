﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Admin.Validators.WorkShifts;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.WorkShifts
{
    [Validator(typeof(WorkShiftValidator))]
    public partial class WorkShiftModel : BaseNopEntityModel
    {
        public WorkShiftModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.WorkShifts.Fields.Name")]
        [AllowHtml]
        public string Name { get; set; }

        [NopResourceDisplayName("Admin.WorkShifts.Fields.StartTime")]
        [AllowHtml]
        public TimeSpan StartTime { get; set; }

        [NopResourceDisplayName("Admin.WorkShifts.Fields.EndTime")]
        [AllowHtml]
        public TimeSpan EndTime { get; set; }

        [NopResourceDisplayName("Admin.WorkShifts.Fields.StoreId")]
        [AllowHtml]
        public int StoreId { get; set; }

        [NopResourceDisplayName("Admin.WorkShifts.Fields.Published")]
        [AllowHtml]
        public bool Published { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public IList<SelectListItem> AvailableStores { get; set; }

        public string StoreName { get; set; }
        public string StartTimeStr { get; set; }
        public string EndTimeStr { get; set; }
    }

    public partial class WorkShiftLocalizedModel : ILocalizedModelLocal
    {
        public int LanguageId { get; set; }
    }
}
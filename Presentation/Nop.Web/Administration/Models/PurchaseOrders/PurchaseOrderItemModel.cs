﻿namespace Nop.Admin.Models.PurchaseOrders
{
    public class PurchaseOrderItemModel
    {
        public int PurchaseOrderId { get; set; }
        
        public int ProductId { get; set; }
        
        public string ProductSku { get; set; }
        
        public string ProductName { get; set; }
        
        public int ProductQuantity { get; set; }

        public string VendorName { get; set; }
    }
}
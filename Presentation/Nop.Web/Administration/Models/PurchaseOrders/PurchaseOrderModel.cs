﻿using System;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.PurchaseOrders
{
    public class PurchaseOrderModel : BaseNopEntityModel
    {
        public string Description { get; set; }
        
        public int TotalQuantity { get; set; }

        public DateTime CreatedOnUtc { get; set; }
    }
}
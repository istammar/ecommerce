﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Nop.Web.Framework;

namespace Nop.Admin.Models.PurchaseOrders
{
    public class PendingOrderListModel
    {
        public PendingOrderListModel()
        {
            OrderDate = DateTime.Now;
            WorkShiftList = new List<SelectListItem>();
        }

        [NopResourceDisplayName("Admin.PendingOrders.List.WorkShift")]
        [AllowHtml]
        public int WorkShift { get; set; }

        [NopResourceDisplayName("Admin.PendingOrders.List.OrderDate")]
        [AllowHtml]
        [UIHint("DateNullable")]
        public DateTime OrderDate { get; set; }

        public List<SelectListItem> WorkShiftList { get; set; }
    }
}
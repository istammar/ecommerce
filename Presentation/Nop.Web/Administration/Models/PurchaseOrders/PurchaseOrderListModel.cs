﻿using System;
using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework;

namespace Nop.Admin.Models.PurchaseOrders
{
    public class PurchaseOrderListModel
    {
        [NopResourceDisplayName("Admin.Competitors.List.ProductName")]
        public string ProductName { get; set; }

        [NopResourceDisplayName("Admin.Competitors.List.Date")]
        [UIHint("DateNullable")]
        public DateTime? FilterDate { get; set; }
    }
}
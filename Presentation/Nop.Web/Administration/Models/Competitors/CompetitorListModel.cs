﻿using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc;

namespace Nop.Admin.Models.Competitors
{
    public partial class CompetitorListModel : BaseNopModel
    {
        [NopResourceDisplayName("Admin.Competitors.List.SearchName")]
        [AllowHtml]
        public string SearchName { get; set; }
    }
}
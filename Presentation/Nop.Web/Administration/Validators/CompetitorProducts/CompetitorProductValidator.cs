﻿using FluentValidation;
using FluentValidation.Results;
using Nop.Admin.Models.CompetitorProducts;
using Nop.Core.Domain.Competitors;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.CompetitorProducts
{
    public partial class CompetitorProductValidator : BaseNopValidator<CompetitorProductModel>
    {
        public CompetitorProductValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.CompetitorId).NotNull().WithMessage(localizationService.GetResource("Admin.CompetitorProducts.Fields.CompetitorId.Required"));

            RuleFor(x => x.ProductId).GreaterThan(0).WithMessage(localizationService.GetResource("Admin.CompetitorProducts.Fields.ProductId.Required")).NotNull().WithMessage(localizationService.GetResource("Admin.CompetitorProducts.Fields.ProductId.Required"));
            RuleFor(x => x.Price).GreaterThan(0).NotNull().WithMessage(localizationService.GetResource("Admin.CompetitorProducts.Fields.Price.Required"));
            RuleFor(x => x.PageSizeOptions).Must(ValidatorUtilities.PageSizeOptionsValidator).WithMessage(localizationService.GetResource("Admin.CompetitorProducts.Fields.PageSizeOptions.ShouldHaveUniqueItems"));
            Custom(x =>
            {
                if (!x.AllowCustomersToSelectPageSize && x.PageSize <= 0)
                    return new ValidationFailure("PageSize", localizationService.GetResource("Admin.CompetitorProducts.Fields.PageSize.Positive"));

                return null;
            });

            SetDatabaseValidationRules<CompetitorProduct>(dbContext);
        }
    }
}
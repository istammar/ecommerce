﻿using FluentValidation;
using Nop.Admin.Models.WorkShifts;
using Nop.Core.Domain.WorkShifts;
using Nop.Data;
using Nop.Services.Localization;
using Nop.Web.Framework.Validators;

namespace Nop.Admin.Validators.WorkShifts
{
    public partial class WorkShiftValidator : BaseNopValidator<WorkShiftModel>
    {
        public WorkShiftValidator(ILocalizationService localizationService, IDbContext dbContext)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.WorkShifts.Fields.Name.Required"));

            RuleFor(x => x.StartTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.WorkShifts.Fields.StartTime.Required"));
            RuleFor(x => x.EndTime).NotEmpty().WithMessage(localizationService.GetResource("Admin.WorkShifts.Fields.EndTime.Required"));
            RuleFor(x => x.StoreId).NotEmpty().WithMessage(localizationService.GetResource("Admin.WorkShifts.Fields.StoreId.Required"));
            
            SetDatabaseValidationRules<WorkShift>(dbContext);
        }
    }
}
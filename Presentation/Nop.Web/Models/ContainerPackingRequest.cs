﻿using System.Collections.Generic;
using CromulentBisgetti.ContainerPacking.Entities;

namespace Nop.Web.Models
{
    public class ContainerPackingRequest
    {
        public ContainerPackingRequest()
        {
            Containers = new List<Container>();
            ItemsToPack = new List<Item>();
            AlgorithmTypeIDs = new List<int>();
        }
        public List<Container> Containers { get; set; }

        public List<Item> ItemsToPack { get; set; }

        public List<int> AlgorithmTypeIDs { get; set; }
    }
}
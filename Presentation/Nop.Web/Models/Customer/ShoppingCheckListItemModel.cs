﻿using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Customer
{
    public class ShoppingCheckListItemModel : BaseNopEntityModel
    {
        public int ShoppingCheckListId { get; set; }

        public string ProductType { get; set; }

        public int Quantity { get; set; }

        public int OldQuantity { get; set; }

        public bool Deleted { get; set; }
    }
}
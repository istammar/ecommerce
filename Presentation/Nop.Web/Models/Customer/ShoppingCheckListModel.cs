﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Customer
{
    public class ShoppingCheckListModel : BaseNopEntityModel
    {
        public string Name { get; set; }

        public int CustomerId { get; set; }

        public bool Deleted { get; set; }

        public DateTime CreatedOnUtc { get; set; }

        public DateTime UpdatedOnUtc { get; set; }

        public List<ShoppingCheckListItemModel> ShoppingCheckListItems { get; set; }

        public int NumberOfCheckListItems { get; set; }
    }

    public class ShoppingCheckListModelList
    {
        public List<ShoppingCheckListModel> ShoppingCheckLists { get; set; }
    }

    //public class ShoppingCheckListModel
    //{
    //    public ShoppingCheckListModel()
    //    {
    //        Categories = new List<CategorySimpleModel>();
    //    }

    //    public List<CategorySimpleModel> Categories { get; set; }
    //}
}
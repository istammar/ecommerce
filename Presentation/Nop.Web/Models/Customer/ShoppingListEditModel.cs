﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Services.Dto;
using Nop.Web.Framework;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.Customer
{
    
    public class ShoppingListEditModel 
    {
        public ShoppingListEditModel()
        {
            Categories = new List<CategorySimpleModel>();
        }

        public List<CategorySimpleModel> Categories { get; set; }
        public List<int> AddedProductIds { get; set; }

        public string CurrencySymbol { get; set; }

        public int ShoppingListId { get; set; }

        [NopResourceDisplayName("Account.ShoppingList.Name")]
        [AllowHtml]
        public string Name { get; set; }

        public List<ShoppingEditListDto> shoppingListItems { get; set; }

        //product fields for autocompete
        public int ProductId { get; set; }

        [NopResourceDisplayName("Account.ShoppingList.ProductName")]
        public string ProductName { get; set; }
    }
}
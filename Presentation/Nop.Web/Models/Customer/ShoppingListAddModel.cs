﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Core.Domain.Customers;
using Nop.Web.Framework;
using System.Web.Mvc;
using Nop.Web.Validators.Customer;
using FluentValidation.Attributes;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.Customer
{
    [Validator(typeof(ShoppingListValidator))]
    public partial class ShoppingListAddModel : BaseNopEntityModel
    {
        public ShoppingListAddModel()
        {
            shoppingListItems = new List<ShoppingListItem>();
            Categories = new List<CategorySimpleModel>();
        }

        public List<ShoppingListItem> shoppingListItems { get; set; }

        public List<CategorySimpleModel> Categories { get; set; }

        public List<int> AddedProductIds { get; set; }

        [NopResourceDisplayName("Account.ShoppingList.Name")]
        [AllowHtml]
        public string Name { get; set; }

        public int CustomerId { get; set; }


        //product fields for autocompete
        public int ProductId { get; set; }

        [NopResourceDisplayName("Account.ShoppingList.ProductName")]
        public string ProductName { get; set; }

        public string CurrencySymbol { get; set; }
    }
}
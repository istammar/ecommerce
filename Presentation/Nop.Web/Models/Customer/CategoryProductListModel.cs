﻿using System.Collections.Generic;
using Nop.Web.Models.Catalog;

namespace Nop.Web.Models.Customer
{
    public class CategoryProductListModel
    {
        public CategoryProductListModel()
        {
            Categories = new List<CategorySimpleModel>();
        }

        public List<CategorySimpleModel> Categories { get; set; }
    }
}
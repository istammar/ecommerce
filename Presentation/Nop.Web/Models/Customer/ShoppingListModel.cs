﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class ShoppingListItemViewModel
    {
        public Boolean Delete { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public int Quantity { get; set; }
    }

    public class ShoppingListModel
    {
        public List<ShoppingListItemViewModel> shoppingList = new List<ShoppingListItemViewModel>();
    }
}
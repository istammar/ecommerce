﻿using Nop.Admin.Models.PurchaseOrders;
using Nop.Core.Domain.Orders;

namespace Nop.Web.Factories
{
    public partial interface IPurchaseOrderModelFactory
    {
        /// <summary>
        /// Prepare the product specification models
        /// </summary>
        /// <param name="orderItem">OrderItem</param>
        /// <returns>Purchase Order Model</returns>
        PurchaseOrderModel PreparePurchaseOrderModel(OrderItem orderItem);
    }
}

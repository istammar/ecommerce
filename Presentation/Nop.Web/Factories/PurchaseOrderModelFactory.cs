﻿using Nop.Admin.Models.PurchaseOrders;
using Nop.Core.Domain.Orders;
using Nop.Services.Catalog;

namespace Nop.Web.Factories
{
    public class PurchaseOrderModelFactory : IPurchaseOrderModelFactory
    {
        #region Fields

        private readonly IProductService _productService;

        #endregion

        #region Constructors

        public PurchaseOrderModelFactory(IProductService productService)
        {
            this._productService = productService;
        }

        #endregion

        #region Methods

        public virtual PurchaseOrderModel PreparePurchaseOrderModel(OrderItem orderItem)
        {
            var product = _productService.GetProductById(orderItem.ProductId);

            PurchaseOrderModel model = new PurchaseOrderModel();
            //{
            //    ProductId = orderItem.ProductId,
            //    ProductName = product.Name,
            //    Quantity = orderItem.Quantity
            //};
            return model;
        }

        #endregion
    }
}